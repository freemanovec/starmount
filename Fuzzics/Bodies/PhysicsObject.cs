﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Text;
using Fuzzics.Worlds;
using VelcroPhysics.Dynamics;
using VelcroPhysics.Primitives;

namespace Fuzzics.Bodies
{
    public sealed class PhysicsObject
    {
        public static PhysicsObject FromBody(Body body, Worlds.World world, float sizeX, float sizeY) => new PhysicsObject(body, world, sizeX, sizeY);

        private Body _body;
        private Worlds.World _world;

        private PhysicsObject() { }

        public float SizeX { get; }
        public float SizeY { get; }

        internal PhysicsObject(Body body, Worlds.World world, float sizeX, float sizeY)
        {
            _body = body;
            _world = world;
            _body.OnCollision += (fixtureA, fixtureB, contact) =>
            {
                OnCollisionEventHandler?.Invoke(fixtureA, fixtureB, contact);
            };
            SizeX = sizeX;
            SizeY = sizeY;
        }

        ~PhysicsObject()
        {
            Debug.WriteLine($"Destructor of PO called, removing body from world");
            _world.DestroyBody(_body);
        }

        public Vector2 Position
        {
            get => _body.Position.Downscale();
            set => _body.Position = value.Upscale();
        }

        public Vector2 Velocity
        {
            get => _body.LinearVelocity.Downscale();
            set => _body.LinearVelocity = value.Upscale();
        }

        public float Rotation
        {
            get => _body.Rotation;
            set => _body.Rotation = value;
        }

        public float Inertia
        {
            get => _body.Inertia.Downscale();
            set => _body.Inertia = value.Upscale();
        }

        public float AngularVelocity
        {
            get => _body.AngularVelocity.Downscale();
            set => _body.AngularVelocity = value.Upscale();
        }

        public float Mass
        {
            get => _body.Mass.Downscale();
            set => _body.Mass = value.Upscale();
        }
        public Vector2 LocalCenter => _body.LocalCenter.Downscale();
        public Vector2 WorldCenter => _body.WorldCenter.Downscale();
        public bool Awake => _body.Awake;

        public void ApplyForce(Vector2 force, Vector2 point) => _body.ApplyForce(force.Upscale(), point.Upscale());
        public void ApplyForce(Vector2 force) => ApplyForce(force.Upscale(), _body.WorldCenter);
        public void ApplyImpulse(Vector2 impulse, Vector2 point) => _body.ApplyLinearImpulse(impulse.Upscale(), point.Upscale());
        public void ApplyImpulse(Vector2 impulse) => _body.ApplyLinearImpulse(impulse.Upscale(), _body.WorldCenter);
        public void ApplyTorque(float torque) => _body.ApplyTorque(torque.Upscale());
        public void ApplyTorqueImpulse(float impulse) => _body.ApplyAngularImpulse(impulse.Upscale());

        internal VelcroPhysics.Collision.Handlers.OnCollisionHandler OnCollisionEventHandler; // TODO custom handling of collisions
        public void RegisterOnCollision(VelcroPhysics.Collision.Handlers.OnCollisionHandler onCollisionHandler) => OnCollisionEventHandler += onCollisionHandler;

    }
}
