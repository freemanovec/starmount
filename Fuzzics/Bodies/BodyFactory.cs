﻿using System;
using System.Collections.Generic;
using System.Text;
using VelcroPhysics.Collision.Filtering;
using VelcroPhysics.Dynamics;
using VelcroPhysics.Primitives;

namespace Fuzzics.Bodies
{
    public class BodyFactory
    {

        private World _world;

        internal BodyFactory(World world)
        {
            _world = world;
        }

        public Body CreateGroundBodyBox(
            float posX,
            float posY,
            float side = 1f,
            bool sensor = false,
            float? friction = null
        )
        {
            Body body = CreateBoxBody((new Vector2(posX, posY)).Upscale(), side.Upscale(), 1000f.Upscale(), BodyType.Static);

            body.IsSensor = sensor;
            if (friction.HasValue)
                body.Friction = friction.Value.Upscale();
            return body;
        }

        public Body CreateDynamicBodyBox(
            float posX,
            float posY,
            float side = 1f,
            float density = 500f,
            float? friction = null,
            bool ignoreGravity = false
        )
        {
            Body body = CreateBoxBody((new Vector2(posX, posY)).Upscale(), side.Upscale(), density.Upscale(), BodyType.Dynamic);            
            if (friction.HasValue)
                body.Friction = friction.Value.Upscale();
            body.IgnoreGravity = ignoreGravity;
            return body;
        }

        private Body CreateBoxBody(Vector2 position, float side, float density, BodyType bType)
        {
            Body body = VelcroPhysics.Factories.BodyFactory.CreateRectangle(
                _world,
                side,
                side,
                density,
                position,
                0,
                bType
                );

            body.CollisionGroup = 1;
            body.CollidesWith = Category.All;
            body.CollisionCategories = Category.All;

            return body;
        }

        public Body CreateGroundBodyCircle(
            float posX,
            float posY,
            float diameter = 1f,
            bool sensor = false,
            float? friction = null
        )
        {
            Body body = CreateCircleBody((new Vector2(posX, posY)).Upscale(), diameter.Upscale(), 1000f.Upscale(), BodyType.Static);

            body.IsSensor = sensor;
            if (friction.HasValue)
                body.Friction = friction.Value.Upscale();

            return body;
        }

        public Body CreateDynamicBodyCircle(
            float posX,
            float posY,
            float diameter = 1f,
            float density = 500f,
            float? friction = null,
            bool ignoreGravity = false
        )
        {
            Body body = CreateCircleBody((new Vector2(posX, posY)).Upscale(), diameter.Upscale(), 1000f.Upscale(), BodyType.Dynamic);
            
            if (friction.HasValue)
                body.Friction = friction.Value.Upscale();
            body.IgnoreGravity = ignoreGravity;

            return body;
        }

        private Body CreateCircleBody(Vector2 position, float diameter, float density, BodyType bType)
        {
            Body body = VelcroPhysics.Factories.BodyFactory.CreateCircle(
                _world,
                diameter / 2,
                density,
                position,
                bType
                );

            body.CollisionGroup = 1;
            body.CollidesWith = Category.All;
            body.CollisionCategories = Category.All;

            return body;
        }

    }
}
