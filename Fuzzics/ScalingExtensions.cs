﻿using System;
using System.Collections.Generic;
using System.Text;
using VelcroPhysics.Primitives;

namespace Fuzzics
{
    internal static class ScalingExtensions
    {
        public static Vector2 Upscale(this Vector2 inp) => inp * Constants.UPSCALE_FACTOR;
        public static Vector2 Downscale(this Vector2 inp) => inp / Constants.UPSCALE_FACTOR;
        public static float Upscale(this float inp) => inp * Constants.UPSCALE_FACTOR;
        public static float Downscale(this float inp) => inp / Constants.UPSCALE_FACTOR;
    }
}
