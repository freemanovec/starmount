﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Fuzzics.Configurations
{
    public struct GravityConfiguration
    {

        public float ForceX;
        public float ForceY;

        public GravityConfiguration(
            float forceX = 0f,
            float forceY = -9.81f
        )
        {
            ForceX = forceX;
            ForceY = forceY;
        }

    }
}
