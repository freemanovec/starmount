﻿using Fuzzics.Worlds;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Text;
using Fuzzics.Bodies;
using Fuzzics.Configurations;

namespace Fuzzics
{
    public sealed class Engine
    {

        private List<World> _worlds = new List<World>();

        public World Setup(GravityConfiguration gravityConfiguration)
        {
            World w = new World(gravityConfiguration);
            if (_worlds.Contains(w))
                return null;
            _worlds.Add(w);
            return w;
        }

        public void Teardown(World world)
        {
            if(!_worlds.Contains(world))
            {
                Debug.WriteLine($"World with ID {world} is not registered");
                return;
            }

            _worlds.Remove(world);
        }

        public void Tick(float duration)
        {
            foreach (World world in _worlds)
                world.Tick(duration);
        }

    }
}
