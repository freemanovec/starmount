﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Text;
using Fuzzics.Bodies;
using Fuzzics.Configurations;
using VelcroPhysics.Dynamics;
using VelcroPhysics.Primitives;

namespace Fuzzics.Worlds
{
    public class World
    {

        private const int POSITION_ITERATIONS = 1;
        private const int VELOCITY_ITERATIONS = 8;

        private VelcroPhysics.Dynamics.World _world;
        
        public BodyFactory BodyFactory { get; }
        
        internal World(GravityConfiguration gravityConfig)
        {
            Vector2 gravity = new Vector2(gravityConfig.ForceX, gravityConfig.ForceY);
            _world = new VelcroPhysics.Dynamics.World(gravity);

            BodyFactory = new BodyFactory(_world);
        }

        public void Tick(float duration)
        {
            Debug.WriteLine($"Ticking physics world");
            _world.Step(duration);
        }

        public void DestroyBody(Body body)
        {
            Debug.WriteLine($"Destroying body {body}");
            _world.RemoveBody(body);
        }

        private List<Body> Bodies => _world.BodyList;

    }
}
