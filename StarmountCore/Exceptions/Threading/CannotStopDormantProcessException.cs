using System;

namespace StarmountCommon.Exceptions.Threading
{
    public class CannotStopDormantProcessException : Exception
    {
        public CannotStopDormantProcessException(string msg) : base(msg)
        {
        }
    }
}