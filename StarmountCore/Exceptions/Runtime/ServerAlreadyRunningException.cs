﻿using System;
using System.Collections.Generic;
using System.Text;

namespace StarmountCore.Exceptions.Runtime
{
    public sealed class ServerAlreadyRunningException : Exception
    {
        public ServerAlreadyRunningException() { }
        public ServerAlreadyRunningException(string msg) : base(msg) { }
    }
}
