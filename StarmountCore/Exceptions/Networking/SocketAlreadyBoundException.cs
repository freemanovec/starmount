using System;

namespace StarmountCommon.Exceptions.Networking
{
    public class SocketAlreadyBoundException : Exception
    {
        public SocketAlreadyBoundException(string msg) : base(msg)
        {
        }
    }
}