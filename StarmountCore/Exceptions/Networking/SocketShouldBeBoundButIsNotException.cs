using System;

namespace StarmountCommon.Exceptions.Networking
{
    public class SocketShouldBeBoundButIsNotException : Exception
    {
        public SocketShouldBeBoundButIsNotException(string msg) : base(msg)
        {
        }
    }
}