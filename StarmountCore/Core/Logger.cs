﻿using System;
using System.Collections.Generic;
using System.Net;
using System.Text;

namespace StarmountCore.Core
{
    public sealed class Logger
    {
        public static Logger Instance { get; } = new Logger();

        private Logger() { }

        public bool Verbose { get; set; }

        public void Log(object message, bool verbose = false)
        {
            if (verbose && !Verbose)
                return;
            string msg = message.ToString();
            string timestamp = DateTime.Now.ToString("HH:mm:ss");
            string[] lines = msg.Split('\n');
            foreach (string line in lines)
            {
                string fin = $"{timestamp}: {line.TrimEnd()}";
                Console.WriteLine(fin);
            }
        }

        public void Error(object message)
        {
            Console.ForegroundColor = ConsoleColor.Red;
            Log(message, false);
            Console.ForegroundColor = ConsoleColor.White;
        }

    }
}
