﻿using System;
using System.Collections.Generic;
using System.Text;

namespace StarmountCore.Core.Actions.Client2Server
{
    public sealed class PlayerPositionUpdatedRequest
    {

        public float PositionX;
        public float PositionY;

    }
}
