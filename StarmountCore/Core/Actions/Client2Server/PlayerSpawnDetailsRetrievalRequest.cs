﻿using System;
using System.Collections.Generic;
using System.Text;

namespace StarmountCore.Core.Actions.Client2Server
{
    public sealed class PlayerSpawnDetailsRetrievalRequest
    {

        public string UniverseUID;
        public string PlanetUID;
        public string PlayerUID;

    }
}
