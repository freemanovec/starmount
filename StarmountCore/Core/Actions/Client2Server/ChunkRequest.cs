﻿using System;
using System.Collections.Generic;
using System.Text;

namespace StarmountCore.Core.Actions.Client2Server
{
    public sealed class ChunkRequest
    {

        public string UniverseUUID;
        public string PlanetUUID;
        public int ChunkX;
        public int ChunkY;

    }
}
