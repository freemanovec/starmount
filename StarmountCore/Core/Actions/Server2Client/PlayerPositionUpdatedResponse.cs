﻿using System;
using System.Collections.Generic;
using System.Text;
using StarmountCore.World.Entities;

namespace StarmountCore.Core.Actions.Server2Client
{
    public sealed class PlayerPositionUpdatedResponse
    {

        public bool PositionOverride;
        public float PositionOverrideX;
        public float PositionOverrideY;

        public Entity[] NewEntitiesInRange;
        public Entity[] NewEntitiesOutOfRange;

    }
}
