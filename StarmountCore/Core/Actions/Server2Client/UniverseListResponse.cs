﻿using System;
using System.Collections.Generic;
using System.Text;

namespace StarmountCore.Core.Actions.Server2Client
{
    public class UniverseListResponse
    {
        public UniverseListResponseEntry[] Universes;
    }

    public class UniverseListResponseEntry
    {
        public string UUID;
        public string Name;
        public int PlanetCount;
    }
}
