﻿using StarmountCore.World.Tiles;
using System;
using System.Collections.Generic;
using System.Text;

namespace StarmountCore.Core.Actions.Server2Client
{
    public sealed class ChunkResponse
    {

        public TileType[,] TileTypes;

    }
}
