﻿using System;
using System.Collections.Generic;
using System.Text;

namespace StarmountCore.Core.Actions.Server2Client
{
    public sealed class PlayerSpawnDetailsRetrievalResponse
    {

        public float PlayerPositionX;
        public float PlayerPositionY;
        
    }
}
