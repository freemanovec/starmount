﻿using System;
using System.Collections.Generic;
using System.Text;

namespace StarmountCore.Core.Actions.Server2Client
{
    public sealed class PlanetListResponse
    {

        public PlanetListResponseEntry[] Planets;
        public string UniverseUUID;

    }

    public sealed class PlanetListResponseEntry
    {
        public string UUID;
        public string Name;
        public int PlayerCount;
    }
}
