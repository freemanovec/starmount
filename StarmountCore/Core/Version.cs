using System;

namespace StarmountCore.Core
{
    public struct Version
    {
        public static readonly Version CurrentVersion = new Version(0, 0, 1);

        public readonly VersionType Type;
        public readonly UInt16 Major;
        public readonly UInt16 Minor;
        public readonly UInt16 Patch;

        public Version(VersionType type, UInt16 major, UInt16 minor = 0, UInt16 patch = 0)
        {
            Type = type;
            Major = major;
            Minor = minor;
            Patch = patch;
        }

        public override string ToString() => $"{Type}-{Major}.{Minor}.{Patch}";
    }

    public enum VersionType
    {
        Alpha,
        Beta,
        Release
    }
}