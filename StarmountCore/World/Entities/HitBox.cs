﻿using OpenTK;
using StarmountCore.World.Core;
using System;
using System.Collections.Generic;
using System.Text;

namespace StarmountCore.World.Entities
{
    public struct HitBox
    {

        public Vector2 LowerLeftCorner { get; }
        public Vector2 UpperRightCorner { get; }
        public float Rotation { get; }
        
        public HitBox(
            float minX,
            float maxX,
            float minY,
            float maxY,
            float rotation
            )
        {
            LowerLeftCorner = new Vector2(minX < maxX ? minX : maxX, minY < maxY ? minY : maxY);
            UpperRightCorner = new Vector2(maxX > minX ? maxX : minX, maxY > minY ? maxY : minY);
            Rotation = rotation;
        }

        public static HitBox FromRectangleGameObject(GameObject go)
        {
            Vector2 pos = go.Position;
            float rotation = go.Rotation;
            float width = go.Width;
            float height = go.Height;
            float minX = pos.X - width / 2;
            float maxX = pos.X + width / 2;
            float minY = pos.Y - height / 2;
            float maxY = pos.Y + height / 2;

            return new HitBox(
                minX,
                maxX,
                minY,
                maxY,
                rotation
                );
        }

    }
}
