﻿using OpenTK;
using StarmountCore.World.Entities.NPCs;
using System;
using System.Collections.Generic;
using System.Text;

namespace StarmountCore.World.Entities
{
    public sealed class EntityFactory
    {

        private readonly Physics.EngineWrapper _engine;
        private readonly Fuzzics.Worlds.World _world;
        private readonly bool _isServer;

        public EntityFactory(
            Physics.EngineWrapper engine,
            Fuzzics.Worlds.World world,
            bool isServer
            )
        {
            _engine = engine;
            _world = world;
            _isServer = isServer;
        }

        public Bloob CreateNPCBloob(Vector2 position) {
            const float size = 1f;
            const float density = 500f;
            const float friction = 15f;
            const string texture = "assets/textures/entities/npcs/bloob/still";

            VelcroPhysics.Dynamics.Body body = _world.BodyFactory.CreateDynamicBodyBox(
                position.X,
                position.Y,
                size,
                density,
                friction
                );

            string name = $"Bloob {Tools.Randomizer.NextName()}";

            Fuzzics.Bodies.PhysicsObject po = Fuzzics.Bodies.PhysicsObject.FromBody(body, _world, size, size);
            Core.GameObject go = new Core.GameObject(_engine, _world, po, texture, 1, name, _isServer);
            Bloob b = new Bloob(go);

            return b;
        }

    }
}
