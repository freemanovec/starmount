﻿using StarmountCore.World.Core;
using System;
using System.Collections.Generic;
using System.Text;
using StarmountCore.World.Physics;
using OpenTK;

namespace StarmountCore.World.Entities
{
    public class Entity
    {
        // TODO an entity needs to have position, rotation, body (hitbox), controller (AI or maintain velocity (TBA)), name
        
        protected GameObject GameObject { get; }

        public Vector2 Position
        {
            get => GameObject.Position;
            set => GameObject.Position = value;
        }

        public float Rotation
        {
            get => GameObject.Rotation;
            set => GameObject.Rotation = value;
        }

        public HitBox HitBox { get; }

        // TODO add controller

        public string Name => GameObject.Name;

        public Entity() { }

        public Entity(
            GameObject go
            )
        {
            GameObject = go;
            HitBox = HitBox.FromRectangleGameObject(go);
        }

        public void Tick()
        {

        }

    }
}
