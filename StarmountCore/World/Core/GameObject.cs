﻿using Fuzzics.Bodies;
using IDRB.GOs;
using IDRB.Textures;
using OpenTK;
using StarmountCore.Tools;
using StarmountCore.World.Physics;
using System;
using System.Collections.Generic;
using System.Text;

namespace StarmountCore.World.Core
{
    public class GameObject : Named
    {
        private PhysicsObject _physicsObject;
        private TexturedGameObject _texturedObject = null;
        private EngineWrapper _engine;

        public GameObject(
            EngineWrapper engine,
            Fuzzics.Worlds.World world,
            Vector2 position,
            float rotation,
            float side,
            string textureName,
            bool dynamic = true,
            float density = 1000,
            float? friction = 1,
            byte textureScaling = 1,
            string name = null,
            bool ignoreGravity = false,
            GameObjectType goType = GameObjectType.Box,
            bool isServerSide = false
        )
        {
            _engine = engine;
            switch (goType)
            {
                case GameObjectType.Box:
                    _physicsObject = PhysicsObject.FromBody(
                                    dynamic
                                        ? world.BodyFactory.CreateDynamicBodyBox(position.X, position.Y, side, density, friction, ignoreGravity)
                                        : world.BodyFactory.CreateGroundBodyBox(position.X, position.Y, side, false, friction),
                                    world,
                        side,
                        side
                                );
                    break;
                case GameObjectType.Circle:
                    _physicsObject = PhysicsObject.FromBody(
                                   dynamic
                                       ? world.BodyFactory.CreateDynamicBodyCircle(position.X, position.Y, side, density, friction, ignoreGravity)
                                       : world.BodyFactory.CreateGroundBodyCircle(position.X, position.Y, side, false, friction),
                                   world,
                        side,
                        side
                               );
                    break;
            }
            _physicsObject.Rotation = rotation;
            if(!isServerSide)
                _texturedObject = new TexturedGameObject(
                    position,
                    rotation,
                    textureName,
                    new Vector2(side, side),
                    textureScaling: textureScaling
                    );
            _engine.RegisterGameObject(this);
            if (name != null) Name = name;
        }

        public GameObject(
            EngineWrapper engine,
            Fuzzics.Worlds.World world,
            PhysicsObject physicsObject,
            string textureName,
            byte textureScaling = 1,
            string name = null,
            bool isServerSide = false
        )
        {
            _engine = engine;
            _physicsObject = physicsObject;
            if(!isServerSide)
                _texturedObject = new TexturedGameObject(
                    _physicsObject.Position.ToOpenTK(),
                    _physicsObject.Rotation,
                    textureName,
                    new Vector2(_physicsObject.SizeX, _physicsObject.SizeY),
                    1f,
                    textureScaling
                    );
        }

        ~GameObject() => _engine.UnregisterGameObject(this);

        public static implicit operator TexturedGameObject(GameObject go) => go._texturedObject;
        public static implicit operator PhysicsObject(GameObject go) => go._physicsObject;

        public Texture Texture
        {
            set {
                if(_texturedObject != null)
                    _texturedObject.Texture = value;
            }
        }

        public float Height => _texturedObject?.Height ?? _physicsObject.SizeY;
        public float Width => _texturedObject?.Width ?? _physicsObject.SizeX;
        public float Opacity => _texturedObject?.Opacity ?? 1;

        public Vector2 Position
        {
            get
            {
                Vector2 pos = _physicsObject.Position.ToOpenTK();
                if (_texturedObject != null)
                    _texturedObject.Position = pos;
                return pos;
            }
            set
            {
                if (_texturedObject != null)
                    _texturedObject.Position = value;
                _physicsObject.Position = value.ToVelcro();
            }
        }

        public float Rotation
        {
            get
            {
                float rotation = _physicsObject.Rotation;
                if (_texturedObject != null)
                    _texturedObject.Rotation = new Quaternion(rotation, 0, 0);
                return rotation;
            }
            set
            {
                _physicsObject.Rotation = value;
                if (_texturedObject != null)
                    _texturedObject.Rotation = new Quaternion(value, 0, 0);
            }
        }

        public Vector2 Scale => new Vector2(Width, Height);

        public float AngularVelocity
        {
            get => _physicsObject.AngularVelocity;
            set => _physicsObject.AngularVelocity = value;
        }

        public Vector2 LocalCenter => _physicsObject.LocalCenter.ToOpenTK();
        public Vector2 WorldCenter => _physicsObject.WorldCenter.ToOpenTK();

        public float Inertia
        {
            get => _physicsObject.Inertia;
            set => _physicsObject.Inertia = value;
        }

        public float Mass
        {
            get => _physicsObject.Mass;
            set => _physicsObject.Mass = value;
        }

        public Vector2 Velocity
        {
            get => _physicsObject.Velocity.ToOpenTK();
            set => _physicsObject.Velocity = value.ToVelcro();
        }

        public bool Awake => _physicsObject.Awake;
        public bool Asleep => !Awake;

        public void ApplyForce(Vector2 force, Vector2 point) => _physicsObject.ApplyForce(force.ToVelcro(), point.ToVelcro());
        public void ApplyForce(Vector2 force) => _physicsObject.ApplyForce(force.ToVelcro());
        public void ApplyImpulse(Vector2 impulse, Vector2 point) => _physicsObject.ApplyImpulse(impulse.ToVelcro(), point.ToVelcro());
        public void ApplyImpulse(Vector2 impulse) => _physicsObject.ApplyImpulse(impulse.ToVelcro());
        public void ApplyTorque(float torque) => _physicsObject.ApplyTorque(torque);
        public void ApplyTorqueImpulse(float impulse) => _physicsObject.ApplyTorqueImpulse(impulse);

        public void RegisterOnCollision(VelcroPhysics.Collision.Handlers.OnCollisionHandler onCollisionHandler) => _physicsObject.RegisterOnCollision(onCollisionHandler);

        public void Synchronize()
        {
            if (_texturedObject == null) return;
            _texturedObject.Rotation = new Quaternion(_physicsObject.Rotation, 0, 0);
        }

        public override string ToString() => $"GameObject '{Name}'";
    }
}
