using System;
using System.Collections.Generic;

namespace StarmountCore.World.Tiles
{
    public sealed class TileHolder{

        private static TileHolder _instance;
        public static TileHolder Instance {
            get {
                if(_instance == null)
                    _instance = new TileHolder();
                return _instance;
            }
        }

        private Dictionary<TileType, Tile> _tileRegistry;
        private TileFactory _tileFactory;

        private TileHolder(){
            _tileRegistry = new Dictionary<TileType, Tile>();
            _tileFactory = new TileFactory();
        }

        private object _tileRegistryManipulationLock = new object();
        public Tile GetTileForType(TileType type){
            lock(_tileRegistryManipulationLock)
                if(!_tileRegistry.ContainsKey(type))
                    _tileRegistry.Add(type, _tileFactory.Create(type));
            return _tileRegistry[type];
        }

    }
}