﻿using System;
using System.Collections.Generic;
using System.Text;

namespace StarmountCore.World.Tiles
{
    public sealed class Tile
    {
        public string VisibleName;
        public uint Durability;
        public bool Passtrough;
        public bool Visible;
        public TileType Type;

        public Tile(TileType type, string visName, TileDurability dur, bool passtrough = false, bool visible = true) : this(type, visName, (uint)dur, passtrough, visible){}
        
        public Tile(TileType type, string visName, uint dur, bool passtrough = false, bool visible = true){
            Type = type;
            VisibleName = visName;
            Durability = dur;
            Passtrough = passtrough;
            Visible = visible;
        }
    }
}
