namespace StarmountCore.World.Tiles
{
    public enum TileDurability{
        Air = 0,
        AlmostAir = 1,
        EasilyBreakable = 10,
        SomewhatDurable = 35,
        Stronger = 75,
        MuchStronger = 180,
        Hard = 300,
        Ore = 500,
        Resistant = 2500
    }
}