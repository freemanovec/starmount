namespace StarmountCore.World.Tiles
{
    public enum TileType{
        Air,
        Stone,
        Dirt,
        Grass,
        Wood,
        Error,
        Coal,
        Void
    }
}