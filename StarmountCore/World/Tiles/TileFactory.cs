using System;
using System.Collections.Generic;
using System.Text;

namespace StarmountCore.World.Tiles
{
    internal sealed class TileFactory
    {

        public Tile Create(TileType type)
        {
            switch(type){
                case TileType.Air:
                    return new Tile(TileType.Air, "Air", TileDurability.Air, passtrough: true, visible: false);
                case TileType.Void:
                    return new Tile(TileType.Void, "Void", TileDurability.Resistant, passtrough: true, visible: false);
                case TileType.Stone:
                    return new Tile(TileType.Stone, "Stone", TileDurability.Hard);
                case TileType.Dirt:
                    return new Tile(TileType.Dirt, "Dirt", TileDurability.EasilyBreakable);
                case TileType.Grass:
                    return new Tile(TileType.Grass, "Grass", TileDurability.EasilyBreakable);
                case TileType.Wood:
                    return new Tile(TileType.Wood, "Wood", TileDurability.SomewhatDurable);
                case TileType.Coal:
                    return new Tile(TileType.Coal, "Coal", TileDurability.Ore);
                default:
                    return new Tile(TileType.Error, "ERROR", TileDurability.AlmostAir);
            }
        }

    }
}