﻿using Fuzzics;
using Fuzzics.Configurations;
using StarmountCore.World.Core;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Text;

namespace StarmountCore.World.Physics
{
    public sealed class EngineWrapper
    {

        private Engine _engine;
        private List<GameObject> _gos = new List<GameObject>();

        public int GameObjectCount => _gos.Count;

        public EngineWrapper()
        {
            _engine = new Engine();
        }

        public Fuzzics.Worlds.World Setup(GravityConfiguration gravityConfiguration) => _engine.Setup(gravityConfiguration);

        public void Teardown(Fuzzics.Worlds.World world) => _engine.Teardown(world);

        public void RegisterGameObject(GameObject go)
        {
            if (_gos.Contains(go))
            {
                Debug.WriteLine($"Gameobject {go} already registered");
                return;
            }

            _gos.Add(go);
        }

        public void UnregisterGameObject(GameObject go)
        {
            if (!_gos.Contains(go))
            {
                Debug.WriteLine($"Gameobject {go} is not registered, cannot remove");
                return;
            }

            _gos.Remove(go);
        }

        public void Tick(float duration) => _engine.Tick(duration);

        public void Synchronize()
        {
            foreach (GameObject go in _gos)
                go.Synchronize();
        }

    }
}
