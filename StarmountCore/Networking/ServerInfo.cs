using System;

namespace StarmountCore.Networking
{
    public struct ServerInfo
    {
        public string Name { get; }
        public string Description { get; }
        public Version Version { get; }

        public ServerInfo(string name, string description, Version version)
        {
            Name = name;
            Description = description;
            Version = version;
        }
    }
}