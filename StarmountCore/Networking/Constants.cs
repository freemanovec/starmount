﻿using System;
using System.Collections.Generic;
using System.Net;
using System.Text;

namespace StarmountCore.Networking
{
    public static class Constants
    {
        
        public const UInt16 DEFAULT_PORT = 4926;
        public const UInt16 PROTOCOL_TYPE_LENGTH = 10;
        public const char PROTOCOL_SEPARATOR = ';';
        public const int TIMEOUT_SECONDS = 15;
        public const char PROTOCOL_MSG_DELIMITER = '\n';
        public const int CACHED_NETWORK_ITEM_MAX_AGE_SECONDS = 5;

    }
}
