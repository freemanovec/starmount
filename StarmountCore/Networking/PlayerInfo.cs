using StarmountCore.Core;

namespace StarmountCore.Networking
{
    public struct PlayerInfo
    {
        public readonly PlayerType PlayerType;
        public readonly ConnectionType ConnectionType;
        public readonly string Username;
        public readonly Version Version;

        public PlayerInfo(string username, PlayerType playerType, ConnectionType connectionType = ConnectionType.Local)
        {
            Username = username;
            PlayerType = playerType;
            ConnectionType = connectionType;
            Version = Version.CurrentVersion;
        }

        public override string ToString() => $"{ConnectionType} - {Username} ({Version})";
    }

    public enum ConnectionType
    {
        Local,
        Remote
    }

    public enum PlayerType
    {
        Bot,
        Human,
        Broadcast
    }
}