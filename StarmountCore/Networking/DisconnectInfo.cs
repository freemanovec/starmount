namespace StarmountCore.Networking
{
    public struct DisconnectInfo
    {
        public DisconnectCause Cause { get; }
        public string Details { get; }
        
        public DisconnectInfo(DisconnectCause cause = DisconnectCause.Unknown, string details = "")
        {
            Cause = cause;
            Details = details;
        }
        
    }

    public enum DisconnectCause
    {
        Unknown,
        UserSideTermination,
        ConnectionTimeout,
        RejectedServer,
        ServerClosed,
        ClientKicked,
        RejectedClient
    }
}