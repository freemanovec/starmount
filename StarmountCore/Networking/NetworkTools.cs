﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;

namespace StarmountCore.Networking
{
    public static class NetworkTools
    {
        
        public static bool IsEmpty<T>(T inp) where T : new()
        {
            if (inp == null || inp.Equals(default(T)))
                return true;
            FieldInfo[] pis = typeof(T).GetFields();
            foreach(FieldInfo pi in pis)
            {
                object pival = pi.GetValue(inp);
                if (pival == null)
                    continue;
                if (pival != null)
                    return false;
                object pinew = Activator.CreateInstance(pi.FieldType);
                if (pival != pinew)
                    return false;
            }
            return true;
        }

    }
}
