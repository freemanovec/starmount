﻿using System;
using System.Collections.Generic;
using System.Text;

namespace StarmountCore.Networking
{
    public sealed class Packet
    {

        public static Packet Empty = new Packet();

        public string RequestType;
        public string ResponseType;
        public string SerializedRequest;
        public string SerializedResponse;
        
        public override bool Equals(object obj)
        {
            if(obj is Packet sec)
            return (
                RequestType == sec.RequestType &&
                ResponseType == sec.ResponseType &&
                SerializedRequest == sec.SerializedRequest &&
                SerializedResponse == sec.SerializedResponse
                );
            return false;
        }

        public override int GetHashCode() => base.GetHashCode();

        public static bool operator==(Packet a, Packet b) => a.Equals(b);
        public static bool operator !=(Packet a, Packet b) => !(a == b);

        public override string ToString() => $"Packet, request {RequestType} ({SerializedRequest.Length}), response {ResponseType} ({SerializedResponse.Length})";

    }
}
