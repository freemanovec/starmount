﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Text;

namespace StarmountCore.Networking
{
    public class EventManager
    {
        public EventHandler<Packet> PacketProcessor { get; set; } // assign externaly

        protected Dictionary<Type, EventHandler<object>> events = new Dictionary<Type, EventHandler<object>>();

        public void Subscribe<TRequest, TResponse>(EventHandler<TResponse> onRequest) where TRequest : class where TResponse : class
        {
            Type requestType = typeof(TRequest);
            if (!events.ContainsKey(requestType))
                events.Add(requestType, (s, e) => { });
            events[requestType] += (s, e) => {
                if (s is TRequest req && e is TResponse res)
                    onRequest.Invoke(req, res);
            };
        }

        public void Subscribe<TRequest>(EventHandler<TRequest> onRequest) where TRequest : class
        {
            Type requestType = typeof(TRequest);
            if (!events.ContainsKey(requestType))
                events.Add(requestType, (s, e) => { });
            events[requestType] += (s, e) =>
            {
                TRequest req = s as TRequest;
                if (req == null) req = e as TRequest;
                if (req != null)
                    onRequest.Invoke(s, req);
            };
        }

        public Packet ProcessIncoming(Packet packet)
        {
            if (string.IsNullOrEmpty(packet.RequestType) || string.IsNullOrEmpty(packet.SerializedRequest))
                return Packet.Empty;
            Type requestType = typeof(Constants).Assembly.GetType(packet.RequestType);
            object requestObject = JsonConvert.DeserializeObject(packet.SerializedRequest, requestType);
            Type responseType = string.IsNullOrEmpty(packet.ResponseType) ? null : typeof(Constants).Assembly.GetType(packet.ResponseType);
            object responseObject = responseType == null ? null : responseType.GetConstructor(Type.EmptyTypes).Invoke(new object[] { });

            if (requestType != null && events.ContainsKey(requestType))
                events[requestType].Invoke(requestObject, responseObject);

            return new Packet()
            {
                RequestType = responseType == null ? null : responseType.FullName,
                SerializedRequest = responseType == null ? null : JsonConvert.SerializeObject(responseObject, responseType, null),
                ResponseType = null
            };
        }


    }
}
