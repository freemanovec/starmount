﻿using System;
using System.Collections.Generic;
using System.Text;

namespace StarmountCore.Tools
{
    public static class VectorExtensions
    {
        public static System.Numerics.Vector2 ToNumerics(this VelcroPhysics.Primitives.Vector2 inp) => new System.Numerics.Vector2(inp.X, inp.Y);
        public static System.Numerics.Vector2 ToNumerics(this OpenTK.Vector2 inp) => new System.Numerics.Vector2(inp.X, inp.Y);

        public static VelcroPhysics.Primitives.Vector2 ToVelcro(this System.Numerics.Vector2 inp) => new VelcroPhysics.Primitives.Vector2(inp.X, inp.Y);
        public static VelcroPhysics.Primitives.Vector2 ToVelcro(this OpenTK.Vector2 inp) => new VelcroPhysics.Primitives.Vector2(inp.X, inp.Y);

        public static OpenTK.Vector2 ToOpenTK(this System.Numerics.Vector2 inp) => new OpenTK.Vector2(inp.X, inp.Y);
        public static OpenTK.Vector2 ToOpenTK(this VelcroPhysics.Primitives.Vector2 inp) => new OpenTK.Vector2(inp.X, inp.Y);
    }
}
