﻿using System;
using System.Collections.Generic;
using System.Text;
using StarmountCore.World.Tiles;

namespace StarmountCore.Tools
{
    public static class ChunkTools
    {

        public static Tile[,] VoidChunkTiles = GetVoidChunkTiles();

        private static Tile[,] GetVoidChunkTiles()
        {
            Tile[,] toRet = new Tile[WorldConstants.CHUNK_SIZE, WorldConstants.CHUNK_SIZE];
            for (int i = 0; i < WorldConstants.CHUNK_SIZE; i++)
            for (int j = 0; j < WorldConstants.CHUNK_SIZE; j++)
                toRet[i, j] = TileHolder.Instance.GetTileForType(TileType.Void);
            return toRet;
        }

    }
}
