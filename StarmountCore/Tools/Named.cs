﻿using System;
using System.Collections.Generic;
using System.Text;

namespace StarmountCore.Tools
{
    public abstract class Named : UIDd
    {
        public string Name { get; set; }

        public Named() : this(Randomizer.NextName())
        {
        }

        public Named(string name) : this(name, Randomizer.NextUID())
        {
        }

        public Named(string name, string uid) : base(uid)
        {
            Name = name;
        }

    }
}
