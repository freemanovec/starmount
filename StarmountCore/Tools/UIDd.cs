﻿using System;
using System.Collections.Generic;
using System.Text;
using StarmountCore.Exceptions.Runtime;

namespace StarmountCore.Tools
{
    public abstract class UIDd
    {

        private bool _changed = false;

        public string UID
        {
            get => _uid;
            set
            {
                if(_changed)
                    throw new UIDHasAlreadyBeenAssigned();
                _uid = value;
                _changed = true;
            }
        }

        private string _uid;

        public UIDd() : this(Randomizer.NextUID())
        {

        }

        public UIDd(string uid)
        {
            UID = uid;
        }

    }
}
