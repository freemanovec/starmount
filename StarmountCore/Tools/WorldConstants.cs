﻿using System;
using System.Collections.Generic;
using System.Text;

namespace StarmountCore.Tools
{
    public static class WorldConstants
    {
        public const int CHUNK_SIZE = 16;
        public const int MAX_HEIGHT_CHUNKS = 16;
        public const float PHYSICS_RATE = 60;
    }
}
