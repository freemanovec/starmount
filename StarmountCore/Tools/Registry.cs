﻿using System;
using System.Collections.Generic;
using System.Text;

namespace StarmountCore.Tools
{
    public sealed class Registry<T>
    {

        private Dictionary<string, T> _dict;

        public Registry()
        {
            _dict = new Dictionary<string, T>();
        }

        public bool RegisterItem(string uuid, T item)
        {
            if (_dict.ContainsKey(uuid))
                return false;
            _dict.Add(uuid, item);
            return true;
        }

        public T this[string uuid]
        {
            get
            {
                if (_dict.ContainsKey(uuid))
                    return _dict[uuid];
                return default(T);
            }
            set
            {
                if (_dict.ContainsKey(uuid))
                    _dict.Remove(uuid);
                RegisterItem(uuid, value);
            }
        }
        
    }
}
