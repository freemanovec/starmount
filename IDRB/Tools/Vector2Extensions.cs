﻿using OpenTK;
using System;
using System.Collections.Generic;
using System.Text;

namespace IDRB.Tools
{
    internal static class Vector2Extensions
    {

        public static Vector2 WithNewX(this Vector2 inp, float newX) => new Vector2(newX, inp.Y);
        public static Vector2 WithNewY(this Vector2 inp, float newY) => new Vector2(inp.X, newY);

    }
}
