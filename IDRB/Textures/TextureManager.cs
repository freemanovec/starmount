﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Text;
using System.Linq;
using SixLabors.ImageSharp;
using SixLabors.ImageSharp.Formats;
using SixLabors.ImageSharp.PixelFormats;
using IDRB.Exceptions;
using OpenTK.Graphics.OpenGL4;

namespace IDRB.Textures
{
    internal sealed class TextureManager
    {
        public static TextureManager Instance { get; } = new TextureManager();

        internal int ElementsLocation;
        internal uint[] Elements = new uint[]
        {
            0, 1, 2, 2, 3, 0
        };

        private TextureManager()
        {
            ElementsLocation = GL.GenBuffer();
            GL.BindBuffer(BufferTarget.ElementArrayBuffer, ElementsLocation);
            GL.BufferData(
                BufferTarget.ElementArrayBuffer,
                Elements.Length * sizeof(uint),
                Elements,
                BufferUsageHint.StaticDraw
            );
        }

        private readonly Dictionary<string, Texture> _holder = new Dictionary<string, Texture>();

        public void LoadTexture(string name, string path)
        {
            Debug.WriteLine($"Loading texture {name} from {path}");

            if (_holder.ContainsKey(name))
                throw new TextureAlreadyLoadedException();
            using (Image<Rgba32> image = Image.Load(path))
            {
                Texture t = new Texture(name, image, path);
                _holder.Add(name, t);
            }
        }

        public void LoadTexture(Texture texture)
        {
            Debug.WriteLine($"Registering an already assembled texture");

            if(_holder.ContainsKey(texture.Name))
                throw new TextureAlreadyLoadedException();
            _holder.Add(texture.Name, texture);
        }

        public Texture GetTexture(string name)
        {
            if (_holder.ContainsKey(name))
                return _holder[name];
            foreach(KeyValuePair<string, Texture> pair in _holder)
                if(pair.Key.EndsWith(name))
                    return pair.Value;
            return null;
        }

        internal void BindPendingTextures()
        {
            foreach (Texture texture in TexturesPendingBinding)
            {
                texture.TextureLocation = GL.GenTexture();
                
                GL.BindTexture(TextureTarget.Texture2D, texture.TextureLocation);
                GL.TexImage2D(
                    TextureTarget.Texture2D,
                    0,
                    PixelInternalFormat.Rgba,
                    texture.Width,
                    texture.Height,
                    0,
                    PixelFormat.Bgra,
                    PixelType.UnsignedByte,
                    texture.FirstPixel
                    );
                GL.TexParameter(TextureTarget.Texture2D, TextureParameterName.TextureMinFilter, (int)TextureMinFilter.Nearest);
                GL.TexParameter(TextureTarget.Texture2D, TextureParameterName.TextureMagFilter, (int)TextureMagFilter.Nearest);
                GL.TexParameter(TextureTarget.Texture2D, TextureParameterName.TextureWrapS, (int)TextureWrapMode.Repeat);
                GL.TexParameter(TextureTarget.Texture2D, TextureParameterName.TextureWrapT, (int)TextureWrapMode.Repeat);

                texture.State = TextureState.Bound;
            }
        }

        private IEnumerable<Texture> TexturesPendingBinding =>_holder.Values.Where(item => item.State == TextureState.Unbound);

    }
}
