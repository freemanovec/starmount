﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Drawing;
using SixLabors.ImageSharp;
using SixLabors.ImageSharp.Advanced;
using SixLabors.ImageSharp.PixelFormats;

namespace IDRB.Textures
{
    public class Texture
    {
        public string Name { get; private set; }
        public string Filename { get; private set; }
        internal TextureState State { get;set; }
        internal readonly IntPtr FirstPixel;
        internal readonly int[] PixelData;
        internal readonly int Width;
        internal readonly int Height;
        internal int TextureLocation;

        internal unsafe Texture(string name, Image<Rgba32> image, string filename = null)
        {
            Name = name;
            Filename = filename;
            State = TextureState.Unbound;

            Width = image.Width;
            Height = image.Height;
            
            int sX = Width;
            int sY = Height;
            PixelData = new int[sX * sY];
            for(int i = 0; i < sX; i++)
            for (int j = 0; j < sY; j++)
            {
                Rgba32 pixel = image[i, j];
                int comb = (pixel.A << 24) | (pixel.R << 16) | (pixel.G << 8) | (pixel.B);
                int ind = j * sX + i;
                PixelData[ind] = comb;
            }

            fixed (int* ptr = PixelData)
            {
                FirstPixel = (IntPtr) ptr;
            }
        }
    }

    internal enum TextureState
    {
        Invalid,
        Unbound,
        Bound
    }
}
