﻿using System;
using IDRB.GOs;
using IDRB.Textures;
using OpenTK;
using OpenTK.Graphics.OpenGL4;

namespace IDRB.Rendering
{
    internal class GPUObject
    {
        private Vector2 position, scale;
        float rotation;

        public GPUObjectState State = GPUObjectState.WaitingForPush;
        public Vector2 Position
        {
            get => position;
            set
            {
                bool changed = position != value;
                position = value;

                if (changed)
                    CreateModelMatrix();
            }
        }

        private Matrix4 ScaleRotationPair;

        public float Rotation
        {
            get => rotation;
            set
            {
                bool changed = rotation != value;
                rotation = value;

                if (changed)
                {
                    CreateScaleRotationPairMatrix();
                    CreateModelMatrix();
                }
            }
        }

        public Vector2 Scale
        {
            get => scale;
            set
            {
                bool changed = scale != value;
                scale = value;

                if (changed)
                {
                    CreateScaleRotationPairMatrix();
                    CreateModelMatrix();
                }
            }
        }

        public float Opacity;
        public Vector2 Size;
        public Texture Texture;

        internal int BufferLocation;
        internal byte TextureScaling = 1;

        public float[] Vertices
        {
            get
            {
                float height = Size.Y;
                float width = Size.X;
                float texRat = TextureScaling;
                float overscan = TextureScaling == 1 ? .005f : 0;
                return new float[]
                {
                    //PositionX PositionY ColorR ColorG ColorB ColorA TexX TexY
                    -width / 2,  height / 2, 1, 0, 0, 1, overscan, overscan, // top left
                     width / 2,  height / 2, 0, 1, 0, 1, texRat - overscan, overscan, // top right
                     width / 2, -height / 2, 0, 0, 1, 1, texRat - overscan, texRat - overscan, // bottom right
                    -width / 2, -height / 2, 1, 1, 1, 1, overscan, texRat - overscan // bottom left
                };
            }
        }
        
        public Matrix4 ModelMatrix { get; private set; }
        
        public void Update(TexturedGameObject tgo)
        {
            Position = tgo.Position;
            Rotation = tgo.Rotation.Z;
            Scale = tgo.Scale;
            Opacity = tgo.Opacity;
            Size = tgo.VisibleRectangle.Size;
            BufferLocation = tgo.VisibleRectangle.BufferLocation;
            TextureScaling = tgo.VisibleRectangle.TextureScaling;
            
            Texture = tgo.VisibleRectangle.Texture;

            if (State == GPUObjectState.UpToDate)
                State = GPUObjectState.WaitingForRedraw;
        }
        
        private void CreateModelMatrix() => ModelMatrix = 
                ScaleRotationPair *
                Matrix4.CreateTranslation(new Vector3(Position.X, Position.Y, -1));

        private void CreateScaleRotationPairMatrix() => ScaleRotationPair =
                Matrix4.CreateScale(new Vector3(Scale.X, Scale.Y, 1)) *
                Matrix4.CreateRotationZ(Rotation);

    }

    internal enum GPUObjectState
    {
        WaitingForPush,
        UpToDate,
        WaitingForRedraw,
        WaitingForDeletion,
        Deleted
    }
}
