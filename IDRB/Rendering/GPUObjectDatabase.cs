﻿using System;
using System.Collections.Generic;
using System.Text;
using IDRB.GOs;
using System.Linq;

namespace IDRB.Rendering
{
    internal sealed class GPUObjectDatabase
    {
        private readonly Dictionary<TexturedGameObject, TexturedGameObjectGPUObjectPair> _database = new Dictionary<TexturedGameObject, TexturedGameObjectGPUObjectPair>();
        public TexturedGameObjectGPUObjectPair[] Pairs => _database.Values.ToArray();

        private object _pushLock = new object();
        public void PushGO(TexturedGameObject go)
        {
            lock (_pushLock)
            {
                if (_database.ContainsKey(go))
                {
                    // object already in gpu, update properties
                    TexturedGameObjectGPUObjectPair pair = _database[go];
                    GPUObject shadow = pair.Shadow;
                    shadow.Update(go);
                }
                else
                {
                    // object not yet in gpu, register
                    GPUObject gpuo = new GPUObject();
                    gpuo.Update(go);
                    TexturedGameObjectGPUObjectPair pair = new TexturedGameObjectGPUObjectPair(go, gpuo);
                    go.PropertyUpdateCallback = (s, e) => { MarkTGOAndUpdate(s as TexturedGameObject); };
                    _database.Add(go, pair);
                }
            }
        }

        public void FlagGOForDeletion(TexturedGameObject go)
        {
            if (_database.ContainsKey(go))
                _database[go].Shadow.State = GPUObjectState.WaitingForDeletion;
        }
        
        public void Rotate()
        {
            List<TexturedGameObjectGPUObjectPair> flagged = new List<TexturedGameObjectGPUObjectPair>(_database.Count / 5);
            foreach(KeyValuePair<TexturedGameObject, TexturedGameObjectGPUObjectPair> pair in _database)
                if(pair.Value.Shadow.State == GPUObjectState.Deleted)
                    flagged.Add(pair.Value);
            foreach (TexturedGameObjectGPUObjectPair pair in flagged)
                _database.Remove(pair.Source);
        }

        private void MarkTGOAndUpdate(TexturedGameObject tgo)
        {
            TexturedGameObjectGPUObjectPair pair = _database[tgo];
            pair.Shadow.Update(pair.Source);
            pair.Shadow.State = GPUObjectState.WaitingForRedraw;
        }
    }

    internal class TexturedGameObjectGPUObjectPair
    {
        public TexturedGameObject Source;
        public GPUObject Shadow;

        public TexturedGameObjectGPUObjectPair(TexturedGameObject source, GPUObject shadow)
        {
            Shadow = shadow;
            Source = source;
        }
        
    }
}
