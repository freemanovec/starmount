﻿using System;
using System.Collections.Generic;
using System.Text;

namespace IDRB.Rendering
{
    internal class ShaderInfo
    {

        #region boring shader defs
        public const string SHADER_VERTEX = 
            @"
#version 440 core
                   
in vec3 vPosition;
in vec2 vTexCoord;
uniform mat4 modelView;
out vec2 fTexCoord;
uniform mat4 translationMatrix;
 
void main()
{
    gl_Position = modelView * translationMatrix * vec4(vPosition, 1.0);
    fTexCoord = vTexCoord;
}
";

        public const string SHADER_FRAGMENT =
            @"
#version 440 core
 
out vec4 outputColor;
uniform float opacity;

uniform sampler2D vTexture;

in vec2 fTexCoord;
 
void main()
{
    outputColor = texture(vTexture, fTexCoord) * vec4(1, 1, 1, opacity);
}
";
        #endregion

        public Handles Handles = new Handles();
        public VertexAttributes VertexAttributes = new VertexAttributes();
        public FragmentAttributes FragmentAttributes = new FragmentAttributes();
        public ModelAttributes ModelAttributes = new ModelAttributes();

        public bool IsBound => 
            Handles.IsBound &&
            VertexAttributes.IsBound &&
            FragmentAttributes.IsBound &&
            ModelAttributes.IsBound;

    }

    internal class Handles
    {
        public int
            Program = -1,
            Vertex = -1,
            Fragment = -1;

        public bool IsBound =>
            Program != -1 &&
            Vertex != -1 &&
            Fragment != -1;
    }

    internal class VertexAttributes
    {
        public int
            Position = -1;

        public bool IsBound => Position != -1;
    }

    internal class FragmentAttributes
    {
        public int
            TextureCoord = -1,
            Sampler2DTexture = -1,
            Opacity = -1;

        public bool IsBound => 
            TextureCoord != -1 &&
            Sampler2DTexture != -1 &&
            Opacity != -1;
    }

    internal class ModelAttributes
    {
        public int
            View = -1,
            Matrix = -1;

        public bool IsBound => 
            View != -1 &&
            Matrix != -1;
    }
}
