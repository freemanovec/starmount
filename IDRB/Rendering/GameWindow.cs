﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Text;
using System.Threading;
using IDRB.EventArgs;
using IDRB.Exceptions;
using IDRB.Scenery;
using IDRB.Textures;
using OpenTK;
using OpenTK.Graphics;
using OpenTK.Graphics.OpenGL4;

namespace IDRB.Rendering
{
    public sealed class GameWindow
    {

        private EventHandler<RenderFrameEventArgs> _onEventRenderFrame = (_, __) => { };
        private EventHandler<UpdateFrameEventArgs> _onEventUpdateFrame = (_, __) => { };
        private EventHandler<ResizeEventArgs> _onEventResize = (_, __) => { };
        private EventHandler<CloseEventArgs> _onEventClose = (_, __) => { };
        private EventHandler<KeyboardKeyDownEventArgs> _onEventKeyboardKeyDown = (_, __) => { };
        private EventHandler<KeyboardKeyUpEventArgs> _onEventKeyboardKeyUp = (_, __) => { };
        private EventHandler<MouseDownEventArgs> _onEventMouseDown = (_, __) => { };
        private EventHandler<MouseUpEventArgs> _onEventMouseUp = (_, __) => { };
        private EventHandler<MouseMoveEventArgs> _onEventMouseMove = (_, __) => { };
        private EventHandler<MouseWheelEventArgs> _onEventMouseWheel = (_, __) => { };

        private OpenTK.GameWindow _gameWindow;
        public OpenTK.GameWindow UnderlyingGameWindow => _gameWindow;

        public Matrix4 ProjectionMatrix { get; private set; }

        public bool ShouldTerminate { get; private set; } = false;
        private bool _runningThreadAbort = false;

        private Renderer _renderer;
        public Camera Camera = new Camera(Vector2.Zero, Vector2.One, Quaternion.Identity);

        internal GPUObjectDatabase GPUObjectDatabase { get; private set; } = new GPUObjectDatabase();

        private string _title;
        public string Title
        {
            get => _title;
            set
            {
                _title = value;
                _gameWindow.Title = _title;
            }
        }

        private bool _initialized = false;
        private ShaderInfo _shaderInfo = new ShaderInfo();

        public GameWindow(int windowWidth, int windowHeight, Vector3 background, string title = "UNTITLED")
        {
            _renderer = new Renderer(background);

            _gameWindow = new OpenTK.GameWindow(
                windowWidth,
                windowHeight,
                new GraphicsMode(GraphicsMode.Default.ColorFormat, 24, 0, 8),
                title,
                GameWindowFlags.Default,
                DisplayDevice.Default,
                4,
                0,
                GraphicsContextFlags.ForwardCompatible
                );
            _gameWindow.VSync = VSyncMode.Adaptive;

            _title = title;

            _gameWindow.RenderFrame += (s, e) =>{ _onEventRenderFrame.Invoke(this, new RenderFrameEventArgs(e)); };
            _gameWindow.UpdateFrame += (s, e) => { _onEventUpdateFrame.Invoke(this, new UpdateFrameEventArgs(e)); };
            _gameWindow.Resize += (s, e) => { _onEventResize.Invoke(this, new ResizeEventArgs(e)); };
            _gameWindow.Closed += (s, e) => { _onEventClose.Invoke(this, new CloseEventArgs(e)); };
            _gameWindow.KeyDown += (s, e) => { _onEventKeyboardKeyDown.Invoke(this, new KeyboardKeyDownEventArgs(e)); };
            _gameWindow.KeyUp += (s, e) => { _onEventKeyboardKeyUp.Invoke(this, new KeyboardKeyUpEventArgs(e)); };
            _gameWindow.MouseDown += (s, e) => { _onEventMouseDown.Invoke(this, new MouseDownEventArgs(e)); };
            _gameWindow.MouseUp += (s, e) => { _onEventMouseUp.Invoke(this, new MouseUpEventArgs(e)); };
            _gameWindow.MouseMove += (s, e) => { _onEventMouseMove.Invoke(this, new MouseMoveEventArgs(e)); };
            _gameWindow.MouseWheel += (s, e) => { _onEventMouseWheel.Invoke(this, new MouseWheelEventArgs(e)); };

        }

        public void Subscribe<TEventArguments>(EventHandler<TEventArguments> onEvent)
        {
            Debug.WriteLine($"Subscribing to event with args {typeof(TEventArguments).Name}");

            switch (onEvent)
            {
                case EventHandler<RenderFrameEventArgs> ehrdea:
                    _onEventRenderFrame += ehrdea;
                    break;
                case EventHandler<UpdateFrameEventArgs> ehupea:
                    _onEventUpdateFrame += ehupea;
                    break;
                case EventHandler<ResizeEventArgs> ehrea:
                    _onEventResize += ehrea;
                    break;
                case EventHandler<CloseEventArgs> ehcea:
                    _onEventClose += ehcea;
                    break;
                case EventHandler<KeyboardKeyDownEventArgs> ehkkdea:
                    _onEventKeyboardKeyDown += ehkkdea;
                    break;
                case EventHandler<KeyboardKeyUpEventArgs> ehkkuea:
                    _onEventKeyboardKeyUp += ehkkuea;
                    break;
                case EventHandler<MouseDownEventArgs> ehmdea:
                    _onEventMouseDown += ehmdea;
                    break;
                case EventHandler<MouseUpEventArgs> ehmuea:
                    _onEventMouseUp += ehmuea;
                    break;
                case EventHandler<MouseMoveEventArgs> ehmmea:
                    _onEventMouseMove += ehmmea;
                    break;
                case EventHandler<MouseWheelEventArgs> ehmwea:
                    _onEventMouseWheel += ehmwea;
                    break;
            }
        }

        public void Initialize()
        {
            if(_initialized)
                throw new AlreadyInitializedException();

            Debug.WriteLine("Initializing shaders");

            _shaderInfo.Handles.Program = GL.CreateProgram();
            _shaderInfo.Handles.Vertex = GL.CreateShader(ShaderType.VertexShader);
            _shaderInfo.Handles.Fragment = GL.CreateShader(ShaderType.FragmentShader);

            GL.ShaderSource(_shaderInfo.Handles.Vertex, ShaderInfo.SHADER_VERTEX);
            GL.ShaderSource(_shaderInfo.Handles.Fragment, ShaderInfo.SHADER_FRAGMENT);

            GL.CompileShader(_shaderInfo.Handles.Vertex);
            GL.CompileShader(_shaderInfo.Handles.Fragment);

            GL.AttachShader(_shaderInfo.Handles.Program, _shaderInfo.Handles.Vertex);
            GL.AttachShader(_shaderInfo.Handles.Program, _shaderInfo.Handles.Fragment);

            Debug.WriteLine($"Vertex shader log: {GL.GetShaderInfoLog(_shaderInfo.Handles.Vertex)}");
            Debug.WriteLine($"Fragment shader log: {GL.GetShaderInfoLog(_shaderInfo.Handles.Fragment)}");

            GL.LinkProgram(_shaderInfo.Handles.Program);
            Debug.WriteLine($"Program info log: {GL.GetProgramInfoLog(_shaderInfo.Handles.Program)}");

            BindShaderAttributes(_shaderInfo);

            float right = (float) _gameWindow.Width / _gameWindow.Height;
            float left = -right;

            Debug.WriteLine($"Creating orthographic camera with boundaries: Left {left}, Right {right}, Bottom {-1f}, Top {1f}");

            ProjectionMatrix = Matrix4.CreateOrthographicOffCenter(left, right, -1f, 1f, -100f, 100f);

            GL.Enable(EnableCap.Blend);
            GL.BlendFunc(BlendingFactorSrc.SrcAlpha, BlendingFactorDest.OneMinusSrcAlpha);

            GL.UseProgram(_shaderInfo.Handles.Program);

            BindEvents();

            GL.EnableVertexAttribArray(_shaderInfo.FragmentAttributes.TextureCoord);
            GL.EnableVertexAttribArray(_shaderInfo.VertexAttributes.Position);

            _initialized = true;
        }

        private void BindShaderAttributes(ShaderInfo si)
        {
            Debug.WriteLine("Binding shader attributes");

            int GAL(string name) => GL.GetAttribLocation(si.Handles.Program, name);
            int GUL(string name) => GL.GetUniformLocation(si.Handles.Program, name);

            //si.VertexAttributes.Color = GAL("vColor");
            si.VertexAttributes.Position = GAL("vPosition");

            si.FragmentAttributes.TextureCoord = GAL("vTexCoord");
            si.FragmentAttributes.Sampler2DTexture = GUL("vTexture");
            si.FragmentAttributes.Opacity = GUL("opacity");

            si.ModelAttributes.View = GUL("modelView");
            si.ModelAttributes.Matrix = GUL("translationMatrix");

            if (!si.IsBound)
                throw new UnableToBindGLAttributesException();
        }

        private void BindEvents()
        {
            Subscribe<RenderFrameEventArgs>((s, e) =>
            {
                TextureManager.Instance.BindPendingTextures();
                _renderer.OnFrame(e, _shaderInfo, Camera.ViewMatrix, ProjectionMatrix, GPUObjectDatabase);
                _gameWindow.SwapBuffers();
            });

            Subscribe<CloseEventArgs>((s, e) => { _runningThreadAbort = true; });
        }

        public void Run()
        {
            Thread t = new Thread(() =>
            {
                while (!_runningThreadAbort)
                    Thread.Sleep(5);
                Close();
            });
            t.Start();
            _gameWindow.Run(120, 60);
        }
       
        public void Close()
        {
            _runningThreadAbort = true;
            Debug.WriteLine("Closing the underlying GameWindow");
            _gameWindow.Close();
        }

    }
}
