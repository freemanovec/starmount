﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Text;
using System.Threading;
using IDRB.EventArgs;
using IDRB.Textures;
using OpenTK;
using OpenTK.Graphics.OpenGL4;
using SixLabors.ImageSharp;
using SixLabors.ImageSharp.PixelFormats;

namespace IDRB.Rendering
{
    internal sealed class Renderer
    {

        private Vector3 _background { get; }

        public Renderer(Vector3 background) => _background = background;

        public void OnFrame(RenderFrameEventArgs args, ShaderInfo shaderInfo, Matrix4 viewMatrix, Matrix4 projectionMatrix, GPUObjectDatabase database)
        {
            Matrix4 currentCameraMatrix = viewMatrix * projectionMatrix;

            GL.ClearColor(_background.X, _background.Y, _background.Z, 1);
            GL.Clear(ClearBufferMask.ColorBufferBit | ClearBufferMask.DepthBufferBit);

            GL.UniformMatrix4(shaderInfo.ModelAttributes.View, false, ref currentCameraMatrix);
            
            foreach (TexturedGameObjectGPUObjectPair pair in database.Pairs)
            {
                GPUObject gpuo = pair.Shadow;

                if (pair.Source.VisibleRectangle.BufferLocation == -1)
                {
                    pair.Source.VisibleRectangle.BufferLocation = GL.GenBuffer();
                    gpuo.BufferLocation = pair.Source.VisibleRectangle.BufferLocation;
                }

                if (gpuo.State == GPUObjectState.WaitingForDeletion)
                {
                    // TODO clear buffers in gpu
                    gpuo.State = GPUObjectState.Deleted;
                }
                else
                {
                    if (gpuo.Texture.State == TextureState.Unbound)
                        Debug.WriteLine($"Using unbound texture: '{gpuo.Texture.Name}'");
                    if (gpuo.Texture.State == TextureState.Invalid)
                        Debug.WriteLine($"Using invalid texture: '{gpuo.Texture.Name}'");

                    // bind it all
                    GL.BindBuffer(BufferTarget.ArrayBuffer, gpuo.BufferLocation);
                    GL.BindBuffer(BufferTarget.ElementArrayBuffer, TextureManager.Instance.ElementsLocation);
                    GL.BindTexture(TextureTarget.Texture2D, gpuo.Texture.TextureLocation);
                    GL.Uniform1(shaderInfo.FragmentAttributes.Opacity, gpuo.Opacity);

                    float[] vertices = gpuo.Vertices;
                    GL.BufferData(
                        BufferTarget.ArrayBuffer,
                        vertices.Length * sizeof(float),
                        vertices,
                        BufferUsageHint.StaticDraw
                    );

                    GL.VertexAttribPointer(
                        shaderInfo.FragmentAttributes.TextureCoord,
                        2,
                        VertexAttribPointerType.Float,
                        false,
                        8 * sizeof(float),
                        6 * sizeof(float)
                    );

                    Matrix4 modelMatrix = gpuo.ModelMatrix;
                    GL.UniformMatrix4(
                        shaderInfo.ModelAttributes.Matrix,
                        false,
                        ref modelMatrix
                    );
                    
                    GL.VertexAttribPointer(
                        shaderInfo.VertexAttributes.Position,
                        2,
                        VertexAttribPointerType.Float,
                        false,
                        8 * sizeof(float),
                        0
                    );
                    
                    GL.DrawElements(BeginMode.Triangles, 6, DrawElementsType.UnsignedInt, 0);
                    gpuo.State = GPUObjectState.UpToDate;
                }
            }
            
            database.Rotate();
            GL.Flush();

        }

    }
}
