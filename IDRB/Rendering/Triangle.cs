﻿using System;
using System.Collections.Generic;
using System.Text;
using OpenTK;

namespace IDRB.Rendering
{
    internal struct Triangle
    {
        private readonly Vector2 PointA, PointB, PointC;

        public Vector2[] Points => new Vector2[]
        {
            PointA, PointB, PointC
        };
        public int[] Ordinals => new int[]{0, 1, 2};

        public Triangle(Vector2 pointA, Vector2 pointB, Vector2 pointC)
        {
            PointA = pointA;
            PointB = pointB;
            PointC = pointC;
        }

    }
}
