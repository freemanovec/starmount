﻿using System;
using System.Collections.Generic;
using System.Text;
using IDRB.Textures;
using OpenTK;
using OpenTK.Graphics.OpenGL4;

namespace IDRB.GOs
{
    internal class VisibleRectangle
    {

        public Vector2 Size { get; set; }
        public float Opacity { get; set; }
        public Texture Texture { get; set; }
        internal int BufferLocation { get; set; } = -1;
        internal byte TextureScaling { get; set; }

        private object _bufferGenLock = new object();
        public VisibleRectangle(Vector2 size, float opacity, Texture texture, byte textureScaling = 1)
        {
            Size = size;
            Opacity = opacity;
            Texture = texture;
            TextureScaling = textureScaling;
        }

    }
}
