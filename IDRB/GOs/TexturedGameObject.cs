﻿using IDRB.Textures;
using System;
using System.Collections.Generic;
using System.Text;
using IDRB.Rendering;
using IDRB.Tools;
using OpenTK;

namespace IDRB.GOs
{
    public class TexturedGameObject : GameObject
    {
        internal VisibleRectangle VisibleRectangle { get; private set; }

        public float Width
        {
            get => VisibleRectangle.Size.X;
            set
            {
                VisibleRectangle.Size = VisibleRectangle.Size.WithNewX(value); 
                PropertyUpdateCallback?.Invoke(this, null);
            }
        }

        public float Height
        {
            get => VisibleRectangle.Size.Y;
            set
            {
                VisibleRectangle.Size = VisibleRectangle.Size.WithNewY(value);
                PropertyUpdateCallback?.Invoke(this, null);
            }
        }

        public Texture Texture
        {
            set
            {
                VisibleRectangle.Texture = value;
                PropertyUpdateCallback?.Invoke(this, null);
            }
        }

        public TexturedGameObject(Vector2 position, Quaternion rotation, Texture texture, Vector2 size, float opacity = 1f, byte textureScaling = 1) : base(position, rotation, Vector2.One)
        {
            VisibleRectangle = new VisibleRectangle(size, opacity, texture, textureScaling);
        }

        public TexturedGameObject(Vector2 position, float rotation, Texture texture, Vector2 size, float opacity = 1f, byte textureScaling = 1) : base(
            position, rotation, Vector2.One)
        {
            VisibleRectangle = new VisibleRectangle(size, opacity, texture, textureScaling);
        }

        public TexturedGameObject(Vector2 position, Quaternion rotation, string texture, Vector2 size, float opacity = 1f, byte textureScaling = 1) : this(position, rotation, TextureManager.Instance.GetTexture(texture), size, opacity, textureScaling)
        {
        }

        public TexturedGameObject(Vector2 position, float rotation, string texture, Vector2 size, float opacity = 1f, byte textureScaling = 1) : this(position,
            rotation, TextureManager.Instance.GetTexture(texture), size, opacity, textureScaling)
        {
        }

        public override string ToString() => $"TGO with texture '{VisibleRectangle.Texture.Name}' at '{Position}'";

    }
}
