﻿using OpenTK;
using System;
using System.Collections.Generic;
using System.Text;
using OpenTK.Platform.Windows;

namespace IDRB.GOs
{
    public class GameObject
    {
        internal EventHandler PropertyUpdateCallback { get; set; }

        private Vector2 _position;
        public Vector2 Position
        {
            get => _position;
            set
            {
                _position = value;
                PropertyUpdateCallback?.Invoke(this, null);
            }
        }

        private Quaternion _rotation { get; set; }
        public Quaternion Rotation
        {
            get => _rotation;
            set
            {
                _rotation = value;
                PropertyUpdateCallback?.Invoke(this, null);
            }
        }

        private Vector2 _scale;
        public Vector2 Scale
        {
            get => _scale;
            set
            {
                _scale = value;
                PropertyUpdateCallback?.Invoke(this, null);
            }
        }

        private float _opacity = 1;
        public float Opacity
        {
            get => _opacity;
            set
            {
                _opacity = value;
                PropertyUpdateCallback?.Invoke(this, null);
            }
        }

        public GameObject(Vector2 position, Quaternion rotation, Vector2 scale)
        {
            Position = position;
            Rotation = rotation;
            Scale = scale;
        }

        public GameObject(Vector2 position, float rotation, Vector2 scale) : this(position,
            Quaternion.FromEulerAngles(0, 0, rotation), scale)
        {
        }

    }
}
