﻿using System;
using System.Collections.Generic;
using System.Text;

namespace IDRB.EventArgs
{
    public sealed class MouseWheelEventArgs : OpenTK.Input.MouseWheelEventArgs
    {
        public MouseWheelEventArgs(OpenTK.Input.MouseWheelEventArgs ea) : base(ea)
        {
        }
    }
}
