﻿using System;
using System.Collections.Generic;
using System.Text;

namespace IDRB.EventArgs
{
    public sealed class MouseMoveEventArgs : OpenTK.Input.MouseMoveEventArgs
    {
        public MouseMoveEventArgs(OpenTK.Input.MouseMoveEventArgs ea) : base(ea)
        {
        }
    }
}
