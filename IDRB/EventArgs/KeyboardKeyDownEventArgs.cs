﻿using System;
using System.Collections.Generic;
using System.Text;
using OpenTK.Input;

namespace IDRB.EventArgs
{
    public sealed class KeyboardKeyDownEventArgs : KeyboardKeyEventArgs
    {
        public KeyboardKeyDownEventArgs(KeyboardKeyEventArgs ea) : base(ea) { }
    }
}
