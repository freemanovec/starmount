﻿using System;
using System.Collections.Generic;
using System.Text;
using OpenTK;

namespace IDRB.EventArgs
{
    public sealed class RenderFrameEventArgs : FrameEventArgs
    {
        public RenderFrameEventArgs(FrameEventArgs ea) : base(ea.Time)
        {
        }
    }
}
