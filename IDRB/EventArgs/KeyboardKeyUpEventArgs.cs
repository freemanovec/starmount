﻿using System;
using System.Collections.Generic;
using System.Text;
using OpenTK.Input;

namespace IDRB.EventArgs
{
    public sealed class KeyboardKeyUpEventArgs : KeyboardKeyEventArgs
    {
        public KeyboardKeyUpEventArgs(KeyboardKeyEventArgs ea) : base(ea)
        {
        }
    }
}
