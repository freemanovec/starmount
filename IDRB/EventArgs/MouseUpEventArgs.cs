﻿using System;
using System.Collections.Generic;
using System.Text;
using OpenTK.Input;

namespace IDRB.EventArgs
{
    public sealed class MouseUpEventArgs : MouseButtonEventArgs
    {
        public MouseUpEventArgs(MouseButtonEventArgs ea) : base(ea)
        {
        }
    }
}
