﻿using System;
using System.Collections.Generic;
using System.Text;
using OpenTK.Input;

namespace IDRB.EventArgs
{
    public sealed class MouseDownEventArgs : MouseButtonEventArgs
    {
        public MouseDownEventArgs(MouseButtonEventArgs ea) : base(ea)
        {
        }
    }
}
