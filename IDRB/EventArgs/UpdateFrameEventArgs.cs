﻿using System;
using System.Collections.Generic;
using System.Text;
using OpenTK;

namespace IDRB.EventArgs
{
    public sealed class UpdateFrameEventArgs : FrameEventArgs
    {
        public UpdateFrameEventArgs(FrameEventArgs ea) : base(ea.Time)
        {
        }
    }
}
