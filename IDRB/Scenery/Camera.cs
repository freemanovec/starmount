﻿using System;
using System.Collections.Generic;
using System.Text;
using IRDB.Scenery;
using OpenTK;

namespace IDRB.Scenery
{
    public sealed class Camera
    {
        public ICameraTrackable Source { get; set; }

        public Matrix4 ViewMatrix =>
            Matrix4.CreateTranslation(-Source.Position.X, -Source.Position.Y, -50) *
            Matrix4.CreateScale(new Vector3(Source.Zoom)) *
            Matrix4.CreateRotationZ(Source.Rotation);

        public Camera(ICameraTrackable source)
        {
            Source = source;
        }

        public Camera(Vector2 position, Vector2 scale, Quaternion rotation)
        {
            Source = new CameraFixture(position, rotation.Z, scale.LengthFast);
        }

    }
}
