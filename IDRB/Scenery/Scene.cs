﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Text;
using System.Threading;
using IDRB.GOs;
using IDRB.Textures;
using OpenTK;
using OpenTK.Graphics.ES10;
using GameWindow = IDRB.Rendering.GameWindow;

namespace IDRB.Scenery
{
    public sealed class Scene
    {

        private GameWindow _gameWindow;

        private Camera _camera;

        public Camera Camera
        {
            get => _camera;
            set
            {
                _camera = value;
                _gameWindow.Camera = _camera;
            }
        }

        public float FPS => (float)_gameWindow.UnderlyingGameWindow.RenderFrequency;

        public string Title
        {
            get => _gameWindow.Title;
            set => _gameWindow.Title = value;
        }

        public Scene(int windowWidth, int windowHeight, string title, Vector3 background)
        {
            _gameWindow = new GameWindow(windowWidth, windowHeight, background, title);

            Camera = new Camera(Vector2.Zero, Vector2.One, Quaternion.Identity);
        }

        public void Run()
        {
            _gameWindow.Initialize();
            _gameWindow.Run();
        }

        public void Subscribe<TEventArguments>(EventHandler<TEventArguments> onEvent) => _gameWindow.Subscribe(onEvent);

        public void LoadTexture(string name, string path) => TextureManager.Instance.LoadTexture(name, path);

        public void RegisterGameObject(TexturedGameObject go)
        {
            _gameWindow.GPUObjectDatabase.PushGO(go);
        }

        public void DeregisterGameObject(TexturedGameObject go)
        {
            _gameWindow.GPUObjectDatabase.FlagGOForDeletion(go);
        }

        public Texture GetTexture(string name) => TextureManager.Instance.GetTexture(name);

    }
}
