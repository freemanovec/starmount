﻿using System;
using System.Collections.Generic;
using System.Text;
using IRDB.Scenery;
using OpenTK;

namespace IDRB.Scenery
{
    internal class CameraFixture : ICameraTrackable
    {
        public Vector2 Position { get; set; }
        public float Rotation { get; set; }
        public float Zoom { get; set; }

        public CameraFixture(Vector2 position, float rotation, float zoom)
        {
            Position = position;
            Rotation = rotation;
            Zoom = zoom;
        }
    }
}
