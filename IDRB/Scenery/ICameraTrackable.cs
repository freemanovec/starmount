﻿using System;
using System.Collections.Generic;
using System.Text;

namespace IRDB.Scenery
{
    public interface ICameraTrackable : IPositionProvider, IRotationProvider
    {
        float Zoom { get; }
    }
}
