﻿using System;
using System.Collections.Generic;
using System.Text;

namespace IRDB.Scenery
{
    public interface IRotationProvider
    {
        float Rotation { get; }
    }
}
