﻿using System;
using System.Collections.Generic;
using System.Text;
using OpenTK;

namespace IRDB.Scenery
{
    public interface IPositionProvider
    {
        Vector2 Position { get; }
    }
}
