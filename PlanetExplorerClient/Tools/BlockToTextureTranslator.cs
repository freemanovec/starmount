﻿using IDRB.Textures;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using IDRB.Scenery;
using StarmountCore.Tools;
using StarmountCore.World.Tiles;

namespace PlanetExplorerClient.Tools
{
    internal sealed class BlockToTextureTranslator
    {
        public static BlockToTextureTranslator Instance { get; } = new BlockToTextureTranslator();

        private Dictionary<TileType, string> _textureMapping = new Dictionary<TileType, string>()
        {
            { TileType.Stone, "assets/textures/blocks/stone" },
            { TileType.Dirt, "assets/textures/blocks/dirt" },
            { TileType.Grass, "assets/textures/blocks/grass_side" },
            { TileType.Wood, "assets/textures/blocks/log_oak" },
            { TileType.Error, "assets/textures/blocks/error" },
            { TileType.Coal, "assets/textures/blocks/coal_ore" }
        };
        
        private object _textureNameGetterLock = new object();
        public string GetTextureNameForType(Scene scene, TileType tileType)
        {
            lock (_textureNameGetterLock)
            {
                Tile tile = TileHolder.Instance.GetTileForType(tileType);
                if (!tile.Visible)
                    return null;

                if (!_textureMapping.ContainsKey(tileType))
                {
                    Debug.WriteLine($"No texture mapped to tile type '{tileType}'");
                    return null;
                }

                string textureName = _textureMapping[tileType];
                return textureName;
            }
        }
    }
}
