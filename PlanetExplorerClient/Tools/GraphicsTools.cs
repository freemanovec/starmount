﻿using IDRB.Scenery;
using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using System.Text.RegularExpressions;
using SixLabors.ImageSharp;
using SixLabors.ImageSharp.Formats.Png;
using SixLabors.ImageSharp.PixelFormats;

namespace PlanetExplorerClient.Tools
{
    public static class GraphicsTools
    {

        private static string[] SUPPORTED_TEXTURE_EXTENSIONS = 
        {
            ".png",
            ".jpg",
            ".jpeg"
        };

        public static void LoadAllTexturesInDirectory(Scene scene, string dirPath, bool recursive = true)
        {
            if (recursive)
            {
                string[] side = Directory.GetDirectories(dirPath);
                foreach (string s in side)
                    LoadAllTexturesInDirectory(scene, s, recursive);
            }

            string[] sideFiles = Directory.GetFiles(dirPath);
            foreach (string s in sideFiles)
            {
                bool valid = false;
                foreach(string ext in SUPPORTED_TEXTURE_EXTENSIONS)
                    if (s.EndsWith(ext))
                    {
                        valid = true;
                        break;
                    }

                if (!valid)
                    continue;

                string name = s.Replace(Path.DirectorySeparatorChar, '/');
                name = name.Replace("../", "/");
                name = Regex.Replace(name, @"\/+", "/");
                name = name.TrimStart('/');
                string[] split = name.Split(".");
                string textureName = "";
                for (int i = 0; i < split.Length - 1; i++)
                    textureName += split[i]; 
                
                scene.LoadTexture(textureName, s);
            }
        }
        
    }
}
