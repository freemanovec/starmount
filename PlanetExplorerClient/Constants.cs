﻿using PlanetExplorerClient.Exceptions;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;

namespace PlanetExplorerClient
{
    internal static class Constants
    {

        public static string TEXTURE_LOCATION = GetDirectoryInTree("assets") + Path.DirectorySeparatorChar + "textures";
        public const string HOSTNAME = "localhost";


        private static string GetDirectoryInTree(string dirName)
        {
            // foreach child node
            string foundInCurrentLower = FindDirectoryInTreeLower(Directory.GetCurrentDirectory(), dirName);
            if (foundInCurrentLower != null)
                return foundInCurrentLower;

            string foundInCurrentHigher = FindDirectoryInTreeHigher(Directory.GetCurrentDirectory(), dirName);
            if (foundInCurrentHigher != null)
                return foundInCurrentHigher;

            throw new TextureDirectoryNotFoundException();
        }

        private static string FindDirectoryInTreeLower(string sourceDir, string dirName)
        {
            string[] currentDirectories = Directory.GetDirectories(sourceDir);
            foreach (string s in currentDirectories)
            {
                if (Path.GetFileName(s) == dirName)
                    return s;
                string got = FindDirectoryInTreeLower(s, dirName);
                if (got != null)
                    return got;
            }

            return null;
        }

        private static string FindDirectoryInTreeHigher(string sourceDir, string dirName)
        {
            DirectoryInfo currentDirectory = new DirectoryInfo(sourceDir);
            int level = 0;

            while (true)
            {
                DirectoryInfo parent = currentDirectory.Parent;
                if (parent == null)
                    return null;
                level++;
                if (parent.Name == dirName)
                {
                    string s = "";
                    for (int i = 0; i < level; i++)
                        s += ".." + Path.DirectorySeparatorChar;
                    return s;
                }

                string[] side = Directory.GetDirectories(parent.FullName);
                for (int i = 0; i < side.Length; i++)
                {
                    string orig = side[i];
                    string[] split = orig.Split(Path.DirectorySeparatorChar);
                    string last = split.Length > 0 ? split[split.Length - 1] : "";
                    side[i] = last;
                }
                if (side.Contains(dirName))
                {
                    string s = "";
                    for (int i = 0; i < level; i++)
                        s += ".." + Path.DirectorySeparatorChar;
                    s += dirName;
                    return s;
                }

                currentDirectory = parent;
            }
        }

    }
}
