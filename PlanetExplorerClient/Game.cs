﻿using System.Diagnostics;
using System.Threading;
using IDRB.EventArgs;
using IDRB.Scenery;
using OpenTK;
using PlanetExplorerClient.Entities;
using PlanetExplorerClient.Graphics;
using PlanetExplorerClient.Tools;
using PlanetExplorerClient.Worlds;
using StarmountClient.Core;

namespace PlanetExplorerClient
{
    internal sealed class Game
    {
        
        private GameClient _client;
        private Scene _scene;
        private World _world;
        private Player _player;
        private GameObjectHolder _goHolder;
        private GameObjectFactory _goFactory;
        private Camera _camera;
        private EntityManager _entityManager;

        public Game()
        {
            _client = new GameClient();
            _scene = new Scene(1280, 720, "Starmount", new Vector3(1f, 1f, 1f));
            _entityManager = new EntityManager();
            _player = new Player(_client.EventManager, (s, e) => _entityManager.OnEntityCreated(e), (s, e) => _entityManager.OnEntityDestroyed(e));
            _goHolder = new GameObjectHolder(_scene);
            _goFactory = new GameObjectFactory(_scene);
            _camera = new Camera(_player);

            _scene.Subscribe<UpdateFrameEventArgs>(OnUpdate);
            _scene.Subscribe<RenderFrameEventArgs>(OnFrame);
            _scene.Subscribe<KeyboardKeyDownEventArgs>(OnKeyDown);
            _scene.Subscribe<MouseWheelEventArgs>(OnScroll);

            _scene.Camera = _camera;
        }

        public void Start()
        {
            GraphicsTools.LoadAllTexturesInDirectory(_scene, Constants.TEXTURE_LOCATION);
            _client.Connect(Constants.HOSTNAME, StarmountCore.Networking.Constants.DEFAULT_PORT);
            LoadWorld();
            _scene.Run();
            _player.Spawn(_client.Universes[0].Planets[0]);
        }
        
        private void LoadWorld()
        {
            StarmountClient.Core.Elements.Planet planet = _client.Universes[0].Planets[0];

            if (_world == null)
                _world = new World(_goFactory, _goHolder, planet);
            else
                _world.SetTarget(planet);
        }

        private void OnUpdate(object _, UpdateFrameEventArgs args)
        {
            _goHolder.DeregisterAllFromQueue();
            _goHolder.RegisterAllFromQueue();
            _player.OnUpdate();
        }

        private void OnFrame(object _, RenderFrameEventArgs args)
        {
            double fps = 1d / args.Time;
            _scene.Title = $"Starmount Planet Viewer ({(int) fps} FPS)";
        }

        private void OnKeyDown(object _, KeyboardKeyDownEventArgs args)
        {
            _player.OnKeyDown(args);
        }

        private void OnScroll(object _, MouseWheelEventArgs args)
        {
            _player.OnScroll(args.DeltaPrecise);
        }

    }
}