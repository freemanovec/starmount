﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Text;
using IDRB.GOs;
using IDRB.Scenery;
using StarmountCore.Core;

namespace PlanetExplorerClient.Graphics
{
    internal class GameObjectHolder
    {

        private Scene _scene;
        private List<TexturedGameObject> _registered = new List<TexturedGameObject>();
        private Queue<TexturedGameObject> _registrationQueue = new Queue<TexturedGameObject>();
        private Queue<TexturedGameObject> _deregistrationQueue = new Queue<TexturedGameObject>();

        public GameObjectHolder(Scene scene)
        {
            _scene = scene;
        }

        ~GameObjectHolder() => ClearAll();

        private object _registrationLock = new object();
        public void AddGOToRegistrationQueue(TexturedGameObject go)
        {
            lock (_registrationLock)
                _registrationQueue.Enqueue(go);
        }

        private object _deregistrationLock = new object();
        public void AddGOToDeregistrationQueue(TexturedGameObject go)
        {
            lock(_deregistrationLock)
                _deregistrationQueue.Enqueue(go);
        }

        public void RegisterAllFromQueue()
        {
            lock (_registrationLock)
            {
                int len = _registrationQueue.Count;
                for (int i = 0; i < len; i++)
                    RegisterGO(_registrationQueue.Dequeue());
            }
        }

        public void DeregisterAllFromQueue()
        {
            lock (_deregistrationLock)
            {
                int len = _deregistrationQueue.Count;
                for (int i = 0; i < len; i++)
                    DeregisterGO(_deregistrationQueue.Dequeue());
            }
        }

        public void ClearAll()
        {
            if (_registered != null && _scene != null)
            {
                foreach (TexturedGameObject go in _registered)
                    _scene.DeregisterGameObject(go);
                _registered.Clear();
            }
        }

        private void RegisterGO(TexturedGameObject go)
        {
            Logger.Instance.Log($"Registering GO '{go}'", true);
            _scene.RegisterGameObject(go);
            _registered.Add(go);
        }

        private void DeregisterGO(TexturedGameObject go)
        {
            Logger.Instance.Log($"Deregistering GO '{go}'", true);
            _scene.DeregisterGameObject(go);
            if (_registered.Contains(go))
                _registered.Remove(go);
        }
    }
}
