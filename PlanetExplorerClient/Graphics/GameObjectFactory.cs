﻿using System;
using System.Collections.Generic;
using System.Text;
using IDRB.GOs;
using IDRB.Scenery;
using OpenTK;
using PlanetExplorerClient.Tools;
using PlanetExplorerClient.Worlds;
using StarmountCore.World.Tiles;

namespace PlanetExplorerClient.Graphics
{
    internal class GameObjectFactory
    {
        private Scene _scene;

        public GameObjectFactory(Scene scene) => _scene = scene;

        public TexturedGameObject CreateWorldBlock(WorldBlock worldBlock)
        {
            if (!TileHolder.Instance.GetTileForType(worldBlock.Type).Visible)
                return null;

            string textureName = BlockToTextureTranslator.Instance.GetTextureNameForType(_scene, worldBlock.Type);
            float centerPositionOffset = (worldBlock.Size - 1) / 2f;
            float positionX = worldBlock.BasePositionX + centerPositionOffset;
            float positionY = worldBlock.BasePositionY + centerPositionOffset;
            Vector2 position = new Vector2(positionX, positionY);
            
            TexturedGameObject go = new TexturedGameObject(
                position,
                0,
                textureName,
                new Vector2(worldBlock.Size, worldBlock.Size),
                1f,
                (byte)worldBlock.Size
                );

            return go;
        }

    }
}
