﻿using StarmountClient.Core;
using StarmountClient.Core.Elements;
using StarmountCore.World.Tiles;
using System;
using System.Collections.Generic;
using System.Net;
using IDRB.Scenery;
using PlanetExplorerClient.Tools;
using StarmountCore.Core;
using System.Linq;
using System.Threading;

namespace PlanetExplorerClient
{
    class Program
    {
        static void Main(string[] args)
        {
            if (args.Contains("--verbose") || args.Contains("-v"))
            {
                Logger.Instance.Verbose = true;
                Logger.Instance.Log("Being verbose", true);
            }
            
            if (args.Contains("--delay-startup"))
            {
                int delay = 3000;
                Logger.Instance.Log($"Delaying startup by {delay} ms");
                Thread.Sleep(delay);
            }
            
            Logger.Instance.Log("Starting the client");
            Game game = new Game();
            game.Start();
        }
    }
}
