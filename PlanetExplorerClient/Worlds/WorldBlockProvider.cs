﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Text;
using StarmountClient.Core.Elements;
using StarmountCore.Tools;
using StarmountCore.World.Tiles;

namespace PlanetExplorerClient.Worlds
{
    internal class WorldBlockProvider
    {

        private Planet _planet;

        public WorldBlockProvider(Planet planet)
        {
            _planet = planet;
        }

        public WorldBlock[] GetWorldBlocksForChunk(int chunkX, int chunkY)
        {
            Debug.WriteLine($"Calculating world blocks for chunk {chunkX}x{chunkY}");

            Chunk chunk = _planet.GetChunk(chunkX, chunkY);
            List<TileType> presentTileTypes = new List<TileType>();
            Tile[,] tiles = new Tile[WorldConstants.CHUNK_SIZE, WorldConstants.CHUNK_SIZE];
            Array.Copy(chunk.Tiles, tiles, WorldConstants.CHUNK_SIZE * WorldConstants.CHUNK_SIZE);
            for(int i = 0; i < WorldConstants.CHUNK_SIZE; i++)
                for(int j = 0; j < WorldConstants.CHUNK_SIZE; j++)
                    if(!presentTileTypes.Contains(tiles[i, j].Type))
                        presentTileTypes.Add(tiles[i,j].Type);

            int worldBaseX = chunkX * WorldConstants.CHUNK_SIZE;
            int worldBaseY = chunkY * WorldConstants.CHUNK_SIZE;

            List<WorldBlock> worldBlocks = new List<WorldBlock>();
            foreach (TileType tileType in presentTileTypes)
            {
                bool[,] processedMap = new bool[WorldConstants.CHUNK_SIZE, WorldConstants.CHUNK_SIZE];
                for(int i = 0; i < WorldConstants.CHUNK_SIZE; i++)
                for (int j = 0; j < WorldConstants.CHUNK_SIZE; j++)
                    if (tiles[i, j].Type != tileType)
                        processedMap[i, j] = true;

                List<MinBlock> minBlocks = new List<MinBlock>();

                for (int i = 0; i < WorldConstants.CHUNK_SIZE; i++) // x
                for (int j = 0; j < WorldConstants.CHUNK_SIZE; j++) // y
                {
                    if (processedMap[i, j]) // this tile's already done
                        continue;

                    // find maximum tolerable size
                    int max = 1;
                    for (int k = 2; k <= WorldConstants.CHUNK_SIZE - Math.Max(i, j); k++) // current checked size
                    {
                        // let's check the right side and the bottom side for overlaps
                        bool clear = true;
                        for (int l = 0; l < k; l++) // current offset on Y from j
                        {
                            if (processedMap[i + k - 1, j + l])
                            {
                                clear = false;
                                break;
                            }
                        }

                        for (int l = 0; l < k - 1; l++) // current offset on Y from i
                        {
                            if (processedMap[i + l, j + k - 1])
                            {
                                clear = false;
                                break;
                            }
                        }

                        if (clear)
                            max++;
                        else
                            break;
                    }

                    MinBlock block = new MinBlock()
                    {
                        X = i,
                        Y = j,
                        Size = max
                    };
                    for (int k = i; k < i + max; k++)
                    for (int l = j; l < j + max; l++)
                        processedMap[k, l] = true;
                    minBlocks.Add(block);
                }

                foreach (MinBlock minBlock in minBlocks)
                    worldBlocks.Add(new WorldBlock(worldBaseX + minBlock.X, worldBaseY + minBlock.Y, minBlock.Size, tileType));
            }
            
            return worldBlocks.ToArray();
        }

        struct MinBlock
        {
            public int X, Y, Size;
        }

    }
}
