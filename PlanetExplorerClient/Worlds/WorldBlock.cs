﻿using System;
using System.Collections.Generic;
using System.Text;
using StarmountCore.World.Tiles;

namespace PlanetExplorerClient.Worlds
{
    internal struct WorldBlock
    {
        public int BasePositionX;
        public int BasePositionY;
        public int Size;
        public TileType Type;

        public WorldBlock(int x, int y, int size, TileType type)
        {
            BasePositionX = x;
            BasePositionY = y;
            Size = size;
            Type = type;
        }
    }
}
