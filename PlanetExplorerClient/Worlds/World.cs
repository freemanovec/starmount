﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using IDRB.GOs;
using IDRB.Scenery;
using PlanetExplorerClient.Graphics;
using StarmountClient.Core.Elements;
using StarmountCore.Tools;

namespace PlanetExplorerClient.Worlds
{
    internal sealed class World
    {

        private GameObjectFactory _gameObjectFactory;
        private GameObjectHolder _gameObjectHolder;
        private Dictionary<int, ChunkBlock[]> _chunks = new Dictionary<int, ChunkBlock[]>(); // _chunks[chunkX][chunkY]
        private Planet _planet;
        private WorldBlockProvider _worldBlockProvider;
        //private EntityProvider _entityProvider;

        public World(GameObjectFactory gameObjectFactory, GameObjectHolder gameObjectHolder, Planet planet)
        {
            _gameObjectFactory = gameObjectFactory;
            _gameObjectHolder = gameObjectHolder;
            SetTarget(planet);
        }

        /// <summary>
        /// clears tiles, position, resets back to initial state
        /// </summary>
        public void Reset()
        {
            _chunks.Clear();
            _planet = null;
            _worldBlockProvider = null;
        }

        /// <summary>
        /// retargets to the provided planet
        /// </summary>
        /// <param name="planet"></param>
        public void SetTarget(Planet planet)
        {
            if (planet == _planet)
                return;

            Reset();
            _planet = planet;
            _worldBlockProvider = new WorldBlockProvider(planet);

            // load chunks surrounding spawn
            List<Task> pool = new List<Task>();
            for (int i = -40; i <= 40; i++)
            for (int j = 0; j < WorldConstants.MAX_HEIGHT_CHUNKS; j++)
                pool.Add(ChunkUpdate(i, j));
        }

        public bool IsChunkLoaded(int chunkX, int chunkY)
        {
            if (!_chunks.ContainsKey(chunkX))
                return false;
            if (_chunks[chunkX][chunkY] == null)
                return false;
            return true;
        }

        private object _chunksLockSet = new object();
        private object _chunksLockGet = new object();
        private async Task ChunkUpdate(Chunk chunk) => await ChunkUpdate(chunk.ChunkX, chunk.ChunkY);
        private async Task ChunkUpdate(int chunkX, int chunkY)
        {
            await Task.Run(() =>
            {
                lock (_chunksLockSet)
                    if (!_chunks.ContainsKey(chunkX))
                        _chunks[chunkX] = new ChunkBlock[WorldConstants.MAX_HEIGHT_CHUNKS];

                WorldBlock[] blocks = _worldBlockProvider.GetWorldBlocksForChunk(chunkX, chunkY);
                ChunkBlock cb = new ChunkBlock(chunkX, chunkY, blocks, _gameObjectFactory);
                ChunkBlock pres;
                lock (_chunksLockGet)
                    pres = _chunks[chunkX][chunkY];
                if (pres != null)
                {
                    TexturedGameObject[] gos = _chunks[chunkX][chunkY].GOs;
                    foreach (TexturedGameObject go in gos)
                        if (go != null)
                            _gameObjectHolder.AddGOToDeregistrationQueue(go);
                }

                lock (_chunksLockGet)
                    _chunks[chunkX][chunkY] = cb;

                foreach (TexturedGameObject go in cb.GOs)
                    if (go != null)
                        _gameObjectHolder.AddGOToRegistrationQueue(go);
            });
        }

    }
}
