﻿using StarmountClient.Core.Elements;
using System;
using System.Collections.Generic;
using System.Text;
using IDRB.GOs;
using PlanetExplorerClient.Graphics;

namespace PlanetExplorerClient.Worlds
{
    internal class ChunkBlock
    {

        public int ChunkX { get; }
        public int ChunkY { get; }
        public WorldBlock[] Blocks { get; }
        public TexturedGameObject[] GOs { get; }
        public readonly DateTime UpdatedAt;

        public ChunkBlock(int chunkX, int chunkY, WorldBlock[] blocks, GameObjectFactory goFactory)
        {
            ChunkX = chunkX;
            ChunkY = chunkY;
            Blocks = blocks;
            UpdatedAt = DateTime.Now;

            GOs = new TexturedGameObject[Blocks.Length];
            for (int i = 0; i < Blocks.Length; i++)
                GOs[i] = goFactory.CreateWorldBlock(blocks[i]);
        }

    }
}
