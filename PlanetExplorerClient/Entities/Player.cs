﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;
using IDRB.EventArgs;
using IRDB.Scenery;
using OpenTK;
using OpenTK.Input;
using StarmountClient.Core.Elements;
using StarmountClient.Networking;
using StarmountCore.Core;
using StarmountCore.Core.Actions.Client2Server;
using StarmountCore.Core.Actions.Server2Client;
using StarmountCore.Tools;
using StarmountCore.World.Entities;
using ClientPlanet = StarmountClient.Core.Elements.Planet;

namespace PlanetExplorerClient.Entities
{
    internal class Player : UIDd, ICameraTrackable
    {
        private ClientEventManager _eventManager;
        private EventHandler<Entity> _onEntityCreated;
        private EventHandler<Entity> _onEntityDestroyed;

        public Vector2 Position { get; private set; }
        public float Rotation { get; private set; } = 0;
        public float Zoom { get; private set; } = 1;

        public Player(
            ClientEventManager eventManager,
            EventHandler<Entity> onEntityCreated,
            EventHandler<Entity> onEntityDestroyed
        )
        {
            _eventManager = eventManager;
            _onEntityCreated = onEntityCreated;
            _onEntityDestroyed = onEntityDestroyed;
        }

        public void Spawn(ClientPlanet planet)
        {
            PlayerSpawnDetailsRetrievalRequest req = new PlayerSpawnDetailsRetrievalRequest()
            {
                UniverseUID = planet.UniverseUID,
                PlanetUID = planet.UID,
                PlayerUID = UID
            };

            bool retrieved = false;
            _eventManager.Send<PlayerSpawnDetailsRetrievalRequest, PlayerSpawnDetailsRetrievalResponse>(req, (s, e) =>
                {
                    Position = new Vector2(e.PlayerPositionX, e.PlayerPositionY);
                    retrieved = true;
                });

            Logger.Instance.Log("Waiting for the server to provide spawn location");
            while (!retrieved)
                Thread.Sleep(100);
            Logger.Instance.Log($"Received spawn location: {Position}");

            throw new NotImplementedException(); // TODO handle server side
        }

        public void OnKeyDown(KeyboardKeyDownEventArgs args)
        {
            float posDelta = 5f;

            switch (args.Key)
            {
                case Key.A:
                    Position += new Vector2(-posDelta, 0);
                    break;
                case Key.D:
                    Position += new Vector2(posDelta, 0);
                    break;
                case Key.S:
                    Position += new Vector2(0, -posDelta);
                    break;
                case Key.W:
                    Position += new Vector2(0, posDelta);
                    break;
            }
        }
        
        public void OnScroll(float delta)
        {
            float multiplier = 1f + delta / 10f;
            Zoom *= multiplier;
        }

        public void OnUpdate()
        {
            // send updated position to server, expect entity in range updates
            PlayerPositionUpdatedRequest req = new PlayerPositionUpdatedRequest()
            {
                PositionX = Position.X,
                PositionY = Position.Y
            };
            _eventManager.Send<PlayerPositionUpdatedRequest, PlayerPositionUpdatedResponse>(req, (s, e) =>
            {
                if (e.PositionOverride)
                {
                    // the player has made an illegal move, use provided overrides to resync with the server
                    Logger.Instance.Log($"Player has made an illegal move");
                    Position = new Vector2(e.PositionOverrideX, e.PositionOverrideY);
                }

                if (e.NewEntitiesInRange.Length > 0)
                {
                    // we've got some new entities
                    foreach (Entity entity in e.NewEntitiesInRange)
                    {
                        Logger.Instance.Log($"An entity has come in range: {entity}");
                        _onEntityCreated(this, entity);
                    }
                }

                if (e.NewEntitiesOutOfRange.Length > 0)
                {
                    // some entities have gotten out of range
                    foreach (Entity entity in e.NewEntitiesOutOfRange)
                    {
                        Logger.Instance.Log($"An entity has come out of range: {entity}");
                    }
                }
            });

            throw new NotImplementedException(); // TODO handle server side
        }
        
    }
}
