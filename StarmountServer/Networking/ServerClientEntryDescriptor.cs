using System;
using System.Net.Sockets;
using StarmountCore.Networking;

namespace StarmountServer.Networking
{
    internal class ServerClientEntryDescriptor
    {
        public PlayerInfo PlayerInfo;
        public Socket Socket;
        public DateTime LastActivity;

        public ServerClientEntryDescriptor(Socket socket)
        {
            Socket = socket;
            LastActivity = DateTime.Now;
        }
    }
}