using System;
using System.Collections.Generic;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading;
using StarmountCore.Exceptions.Networking;
using StarmountCore.Core.Actions;
using StarmountCore.Networking;
using StarmountCore.Core.Actions.Client2Server;
using Newtonsoft.Json;
using StarmountCore.Networking.TCP;
using StarmountCore.Core;

namespace StarmountServer.Networking.TCP
{
    public class TcpServer : IServerEndpoint
    {
        private SimpleTcpServer _server;
        private List<TcpClient> _connectedClients = new List<TcpClient>();
        private ServerEventManager _eventManager;
        
        public TcpServer(ServerEventManager eventManager)
        {
            _eventManager = eventManager;
        }

        public void Start()
        {
            _eventManager.PacketProcessor += (s, packet) =>
            {
                _server.BroadcastLine(JsonConvert.SerializeObject(packet));
            };
            _server = new SimpleTcpServer();
            _server.Delimiter = (byte)Constants.PROTOCOL_MSG_DELIMITER;
            _server.ClientConnected += OnClientConnect;
            _server.ClientDisconnected += OnClientDisconnect;
            _server.DelimiterDataReceived += OnMessage;
            _server.Start(Constants.DEFAULT_PORT);
        }

        public void Stop()
        {
            _server.Stop();
        }

        private void OnClientConnect(object _, TcpClient client)
        {
            Logger.Instance.Log("A new client has appeared", true);
            if (_connectedClients.Contains(client))
                throw new ClientAlreadyConnectedException();
            
            IPEndPoint endpoint = (IPEndPoint)client.Client.RemoteEndPoint;
            Connect connect = new Connect() {
                Address = endpoint.Address.ToString(),
                Port = endpoint.Port
            };
            Packet artificialInboundPacket = new Packet()
            {
                RequestType = typeof(Connect).FullName,
                SerializedRequest = JsonConvert.SerializeObject(connect)
            };
            _eventManager.ProcessIncoming(artificialInboundPacket);

            _connectedClients.Add(client);
        }

        private void OnClientDisconnect(object _, TcpClient client)
        {
            Logger.Instance.Log("An old client has disappeared", true);
            if (!_connectedClients.Contains(client))
                throw new ClientAlreadyDisconnectedException();


            IPEndPoint endpoint = (IPEndPoint)client.Client.RemoteEndPoint;
            Disconnect disconnect = new Disconnect()
            {
                Address = endpoint.Address.ToString(),
                Port = endpoint.Port
            };
            Packet artificialInboundPacket = new Packet()
            {
                RequestType = typeof(Disconnect).FullName,
                SerializedRequest = JsonConvert.SerializeObject(disconnect)
            };
            _eventManager.ProcessIncoming(artificialInboundPacket);

            _connectedClients.Remove(client);
        }

        private void OnMessage(object _, Message message)
        {
            Logger.Instance.Log($"A wild message has appeared: {message.MessageString.Replace("\n", "")}", true);
            Packet requestPacket = JsonConvert.DeserializeObject<Packet>(message.MessageString);
            Packet responsePacket = _eventManager.ProcessIncoming(requestPacket);
            if (!string.IsNullOrEmpty(responsePacket.SerializedRequest))
                message.ReplyLine(JsonConvert.SerializeObject(responsePacket));
        }
        
    }
}