using StarmountCore.Networking;

namespace StarmountServer.Networking
{
    internal interface IServerEndpoint
    {
        void Start();
        void Stop();
    }
}