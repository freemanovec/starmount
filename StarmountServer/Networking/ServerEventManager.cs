using System;
using Newtonsoft.Json;
using StarmountCore.Networking;

namespace StarmountServer.Networking
{
    public sealed class ServerEventManager : EventManager
    {
        public void Broadcast<TRequest>(TRequest request)
        {
            Type requestType = typeof(TRequest);
            string serializedRequest = JsonConvert.SerializeObject(request);
            Packet packet = new Packet()
            {
                RequestType = requestType.FullName,
                SerializedRequest = serializedRequest
            };
            PacketProcessor?.Invoke(this, packet);
        }
        
    }

}