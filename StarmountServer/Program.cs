﻿using System;
using System.Linq;
using StarmountCore.Core;
using StarmountServer.Core;
using StarmountServer.Core.Planets;
using StarmountServer.Core.Planets.Generators;
using StarmountServer.Core.Universes;
using StarmountServer.Core.Planets.Inhabitants;
using StarmountCore.World.Physics;
using FuzzicsWorld = Fuzzics.Worlds.World;

namespace StarmountServer
{
    class Program
    {
        static void Main(string[] args)
        {
            if (args.Contains("--verbose") || args.Contains("-v"))
            {
                Logger.Instance.Verbose = true;
                Logger.Instance.Log("Being verbose", true);
            }

            Logger.Instance.Log("Starting server");

            EngineWrapper engine = new EngineWrapper();
            GameServer gameServer = new GameServer();
            FuzzicsWorld physicsWorld = engine.Setup(new Fuzzics.Configurations.GravityConfiguration());

            gameServer.LoadUniverse(new Universe(new []
            {
                new Planet(
                    new PrimitiveEarth(),
                    new InhabitanceConfiguration(
                        InhabitanceDensity.Medium,
                        InhabitanceHostility.Extreme
                    ),
                    engine,
                    physicsWorld)
            }));
            
            System.Threading.CancellationTokenSource tokenSource = gameServer.RunAsync(engine);

            bool shouldLoopityLoop = true;
            while (shouldLoopityLoop)
            {
                string command = Console.ReadLine();
                switch (command)
                {
                    case "exit":
                    case "stop":
                    case "quit":
                        tokenSource.Cancel();
                        shouldLoopityLoop = false;
                        break;
                }
            }

            Logger.Instance.Log("Server stopping");
        }
    }
}
