using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using StarmountCore.Core;
using StarmountCore.Core.Actions.Client2Server;
using StarmountCore.Core.Actions.Server2Client;
using StarmountCore.Exceptions.Runtime;
using StarmountCore.Networking;
using StarmountCore.Tools;
using StarmountCore.World.Physics;
using StarmountCore.World.Tiles;
using StarmountServer.Core.Chunks;
using StarmountServer.Core.Planets;
using StarmountServer.Core.Qs;
using StarmountServer.Core.Universes;
using StarmountServer.Networking;
using StarmountServer.Networking.TCP;

namespace StarmountServer.Core
{
    internal sealed class GameServer : Named
    {
        private const int TICKS_PER_SECOND = 20;
        private const int TICK_FRAME = 1000 / TICKS_PER_SECOND;
        private const int TICK_SKIP_AMOUNT = 5 * TICKS_PER_SECOND;

        private readonly ServerEventManager _eventManager;
        private readonly IServerEndpoint _endpoint;

        private Q _q;

        public override string ToString() => $"GameServer '{Name}'";

        internal GameServer()
        {
            _eventManager = new ServerEventManager();
            _endpoint = new TcpServer(_eventManager);
            _q = new Q();
            AssignToEventHandler();
        }
        
        internal void Block(EngineWrapper engine, CancellationToken? ct = null)
        {
            Logger.Instance.Log("Initializing network side");
            _endpoint.Start();
            Logger.Instance.Log("Server tickin'");
            Stopwatch stopwatch = new Stopwatch();
            while (!ct.HasValue || !ct.Value.IsCancellationRequested)
            {
                int duration = TICK_FRAME;
                stopwatch.Restart();
                engine?.Tick(duration);
                _q?.Tick();
                stopwatch.Stop();
                int elapsed = (int)stopwatch.ElapsedMilliseconds;
                int difference = duration - elapsed;
                int tolerance = 3;
                if (difference < -(duration + tolerance))
                {
                    // worse than tolerance, alert
                    Logger.Instance.Log($"Can't keep up, last tick took {-difference} ms (more than the maximum of {duration} ms)");
                }
                else if (difference <= 0)
                {
                    // within tolerance
                }
                else
                {
                    // we can sleep for the difference
                    Thread.Sleep(difference);
                }
            }
            Logger.Instance.Log("Server stopped");
        }

        internal CancellationTokenSource RunAsync(EngineWrapper engine)
        {
            CancellationTokenSource cts = new CancellationTokenSource();
            CancellationToken token = cts.Token;
            Task.Factory.StartNew(() =>
            {
                Block(engine, token);
            }, token);
            return cts;
        }

        internal void LoadUniverse(Universe universe)
        {
            Logger.Instance.Log($"Loading universe '{universe}'", true);
            _q.UniverseManager.LoadUniverse(universe);
        }
        
        private void AssignToEventHandler()
        {
            Logger.Instance.Log($"Subscribing to events", true);
            _eventManager.Subscribe<UniverseListRequest, UniverseListResponse>(ProcessUniverseListRequest);
            _eventManager.Subscribe<PlanetListRequest, PlanetListResponse>(ProcessPlanetListRequest);
            _eventManager.Subscribe<ChunkRequest, ChunkResponse>(ProcessChunkRequest);
        }

        private void ProcessUniverseListRequest(object _, UniverseListResponse response)
        {
            Logger.Instance.Log($"Responding to Universe List Request", true);
            List<Universe> universes = _q.UniverseManager.Universes;
            response.Universes = new UniverseListResponseEntry[universes.Count];
            for (int i = 0; i < response.Universes.Length; i++)
            {
                Universe curU = universes[i];
                UniverseListResponseEntry ulre = new UniverseListResponseEntry()
                {
                    UUID = curU.UID,
                    Name = curU.Name,
                    PlanetCount = curU.PlanetCount
                };
                response.Universes[i] = ulre;
            }
        }

        private void ProcessPlanetListRequest(object _, PlanetListResponse response)
        {
            if (_ is PlanetListRequest request)
            {
                Universe uni = _q.UniverseManager.GetUniverse(request.UniverseUUID);
                Logger.Instance.Log($"Responding to Planet List Request for universe '{uni}'", true);
                if (uni == null)
                    return;
                List<Planet> planets = uni.PlanetManager.Planets;
                response.Planets = new PlanetListResponseEntry[planets.Count];
                for (int i = 0; i < response.Planets.Length; i++)
                {
                    Planet planet = planets[i];
                    PlanetListResponseEntry plre = new PlanetListResponseEntry()
                    {
                        UUID = planet.UID,
                        Name = planet.Name,
                        PlayerCount = planet.PlayerCount
                    };
                    response.Planets[i] = plre;
                }
            }
        }

        private void ProcessChunkRequest(object _, ChunkResponse response)
        {
            if (_ is ChunkRequest request)
            {
                Universe uni = _q.UniverseManager.GetUniverse(request.UniverseUUID);
                if (uni == null)
                {
                    Logger.Instance.Error($"Couldn't get universe for UID '{request.UniverseUUID}'");
                    return;
                }

                Planet planet = uni.PlanetManager.GetPlanet(request.PlanetUUID);
                if (planet == null)
                {
                    Logger.Instance.Error($"Couldn't get planet for UID '{request.PlanetUUID}'");
                    return;
                }

                Logger.Instance.Log($"Responding to Chunk Request for planet '{planet}' at {request.ChunkX}x{request.ChunkY}", true);
                Chunk chnk = planet.GetChunk(request.ChunkX, request.ChunkY);
                response.TileTypes = new TileType[WorldConstants.CHUNK_SIZE, WorldConstants.CHUNK_SIZE];
                for(int i = 0; i < WorldConstants.CHUNK_SIZE; i++)
                for (int j = 0; j < WorldConstants.CHUNK_SIZE; j++)
                {
                    response.TileTypes[i, j] = chnk.GetTile(i, j).Type;
                }
            }
        }

    }
}