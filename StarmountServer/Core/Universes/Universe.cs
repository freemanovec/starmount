﻿using System;
using System.Collections.Generic;
using System.Text;
using StarmountCore.Tools;
using StarmountServer.Core.Planets;

namespace StarmountServer.Core.Universes
{
    internal sealed class Universe : Named
    {
        public int PlanetCount => _planetManager.Planets.Count;

        private readonly PlanetManager _planetManager;
        public PlanetManager PlanetManager => _planetManager;
        private readonly int _seed;

        public Universe(int seed = 0)
        {
            _seed = seed;
            _planetManager = new PlanetManager();
        }

        public Universe(Planet[] planets) : this()
        {
            foreach (Planet p in planets)
                LoadPlanet(p);
        }

        public override string ToString() => $"Universe '{Name}'";

        public bool LoadPlanet(Planet p) => _planetManager.LoadPlanet(p);

        public void Tick() => PlanetManager.Tick();
    }
}
