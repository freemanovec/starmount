﻿using StarmountServer.Core.Planets.Generators;
using System;

namespace StarmountServer.Core.Universes
{
    public sealed class UniverseGenerator
    {
        private readonly int _seed;
        private readonly Random _rand;

        public UniverseGenerator(int? seed = null)
        {
            _rand = seed.HasValue ? new Random(seed.Value) : new Random();
        }

        internal Universe GenerateUniverse(
            // TODO universe properties
            )
        {
            Universe universe = new Universe(_seed);
            IChunkSupplier planetGenerator = new PrimitiveEarth(_seed);
            
            throw new NotImplementedException();
        }
    }
}
