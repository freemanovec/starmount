﻿using System;
using System.Collections.Generic;
using System.Text;

namespace StarmountServer.Core.Universes
{
    internal sealed class UniverseManager
    {
        public List<Universe> Universes { get; private set; }

        public UniverseManager()
        {
            Universes = new List<Universe>();
        }

        public bool LoadUniverse(Universe universe){
            if(Universes.Contains(universe))
                return false;
            Universes.Add(universe);
            return true;
        }

        public Universe GetUniverse(string uid)
        {
            foreach (Universe uni in Universes)
                if (uni.UID == uid)
                    return uni;
            return null;
        }

        internal void Tick() => Universes.ForEach(el => el.Tick());
    }
}
