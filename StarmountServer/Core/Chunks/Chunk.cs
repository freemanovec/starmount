﻿using StarmountCore.World.Tiles;
using System;
using System.Collections.Generic;
using System.Text;

namespace StarmountServer.Core.Chunks
{
    internal sealed class Chunk
    {
        private Tile[,] _tiles;

        // these are the coords in the chunk, hence between 0 (inclusive) and WorldConstants.CHUNK_SIZE (exclusive)
        public Tile GetTile(int x, int y) => _tiles[x, y];
        public Tile this[int x, int y] => GetTile(x, y);

        internal Chunk()
        {
        }

        internal Chunk(Tile[,] tiles) => _tiles = tiles;

        internal void Tick()
        {
            // TODO tick tile entities when tile entities are implemented
        }
    }
}
