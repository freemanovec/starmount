﻿using System;
using System.Collections.Generic;
using System.Text;
using OpenTK;

namespace StarmountServer.Core.Entities
{
    internal sealed class ChunkloadingEntity
    {
        public int ChunkloadingRadius { get; set; }
        public Vector2 CurrentChunk { get; set; }

        public ChunkloadingEntity(Vector2 currentChunk, int chunkloadingRadius = 1)
        {
            ChunkloadingRadius = chunkloadingRadius;
            CurrentChunk = currentChunk;
        }
    }
}