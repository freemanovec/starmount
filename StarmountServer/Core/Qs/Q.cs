﻿using StarmountServer.Core.Universes;
using System;
using System.Collections.Generic;
using System.Text;

namespace StarmountServer.Core.Qs
{
    internal sealed class Q
    {

        private readonly UniverseManager _universeManager;
        public UniverseManager UniverseManager => _universeManager;

        internal bool Alive => true; // TODO Q deaths that terminates the server

        internal Q()
        {
            _universeManager = new UniverseManager();
        }

        internal void Tick() => UniverseManager.Tick();
        
    }
}
