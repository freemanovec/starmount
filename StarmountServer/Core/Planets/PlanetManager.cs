﻿using System;
using System.Collections.Generic;
using System.Text;

namespace StarmountServer.Core.Planets
{
    internal sealed class PlanetManager
    {
        public List<Planet> Planets { get; private set; }

        public PlanetManager(){
            Planets = new List<Planet>();
        }

        public bool LoadPlanet(Planet planet){
            if(Planets.Contains(planet))
                return false;
            Planets.Add(planet);
            return true;
        }

        public Planet GetPlanet(string uid)
        {
            foreach(Planet planet in Planets)
                if (planet.UID == uid)
                    return planet;
            return null;
        }

        public void Tick() => Planets.ForEach(el => el.Tick());
    }
}
