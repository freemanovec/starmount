﻿using System;
using System.Collections.Generic;
using System.Text;
using StarmountCore.World.Tiles;
using StarmountServer.Core.Chunks;
using StarmountServer.Core.Planets.Generators;
using StarmountServer.Tools;
using StarmountServer.Core.Planets.Inhabitants;
using StarmountCore.Tools;
using StarmountCore.World.Physics;
using System.Linq;
using OpenTK;
using StarmountServer.Core.Entities;
using StarmountServer.Tools.Math;

namespace StarmountServer.Core.Planets
{
    internal sealed class Planet : Named
    {

        public int PlayerCount => 0; // TODO make use of

        private IChunkSupplier _chunkSupplier;
        private Dictionary<int, Chunk[]> _chunks;
        private InhabitanceManager _inhabitanceManager;
        private List<ChunkloadingEntity> _chunkloadingEntities;

        public Planet(IChunkSupplier chunkSupplier, EngineWrapper engineWrapper, Fuzzics.Worlds.World world)
            : this(chunkSupplier, InhabitanceConfiguration.Default, engineWrapper, world){}

        public Planet(
            IChunkSupplier chunkSupplier,
            InhabitanceConfiguration inhabitanceConfiguration,
            EngineWrapper engineWrapper,
            Fuzzics.Worlds.World world
        ){
            _chunkSupplier = chunkSupplier ?? throw new ArgumentException(nameof(chunkSupplier));
            _chunks = new Dictionary<int, Chunk[]>();
            _inhabitanceManager = new InhabitanceManager(
                inhabitanceConfiguration, 
                new StarmountCore.World.Entities.EntityFactory(engineWrapper, world, true));
            _inhabitanceManager.Initialize();
            _chunkloadingEntities = new List<ChunkloadingEntity>();
        }

        public override string ToString() => $"Planet '{Name}'";

        public Chunk GetChunk(int x, int y) // uses chunk coords
        {
            if (!_chunks.ContainsKey(x))
                _chunks.Add(x, new Chunk[WorldConstants.MAX_HEIGHT_CHUNKS]);
            if (_chunks[x][y] == null)
                _chunks[x][y] = _chunkSupplier.GetChunkAt(x, y);
            return _chunks[x][y];
        }

        public Tile GetTile(int x, int y){ // uses world coords, not chunk coords
            // get chunk coords from world coords
            int chunk_x = x / WorldConstants.CHUNK_SIZE;
            int chunk_y = y / WorldConstants.CHUNK_SIZE;
            int tile_x = x - chunk_x;
            int tile_y = y - chunk_y;
            return GetChunk(chunk_x, chunk_y).GetTile(tile_x, tile_y);
        }

        public void Tick()
        {
            // tick chunks

            // get chunkloading entities on this planet
            ChunkloadingEntity[] chunkloadingEntities = new ChunkloadingEntity[_chunkloadingEntities.Count];
            _chunkloadingEntities.CopyTo(chunkloadingEntities);

            // foreach entity, get chunks in radius
            foreach (ChunkloadingEntity chunkloadingEntity in chunkloadingEntities)
            {
                int entityChunkloadingRadius = chunkloadingEntity.ChunkloadingRadius;
                Vector2 baseChunkPosition = chunkloadingEntity.CurrentChunk;
                Chunk[] chunks = GetChunksInRange(baseChunkPosition, entityChunkloadingRadius);
                foreach(Chunk chunk in chunks)
                    chunk.Tick();
            }

            // tick inhabitance manager
            _inhabitanceManager.Tick();
        }

        private Chunk[] GetChunksInRange(Vector2 baseChunk, int radius)
        {
            int baseX = (int) baseChunk.X;
            int leftmostX = baseX - radius + 1; // same as baseX when radius == 1, otherwise it's the remaining to the left
            int rightmostX = baseX + radius - 1;
            int baseY = (int) baseChunk.Y;
            int lowermostY = baseY - radius + 1;
            int uppermostY = baseY + radius - 1;
            List<Chunk> foundChunks = new List<Chunk>();
            for (int i = leftmostX; i <= rightmostX; i++)
            {
                for (int j = lowermostY; j <= uppermostY; j++)
                {
                    Chunk curChunk = GetChunk(i, j);
                    Vector2 curPos = new Vector2(i, j);
                    int curDistance = (int)((baseChunk - curPos).LengthFast);
                    if (curDistance <= radius)
                        foundChunks.Add(curChunk);
                }
            }

            return foundChunks.ToArray();
        }

    }
}
