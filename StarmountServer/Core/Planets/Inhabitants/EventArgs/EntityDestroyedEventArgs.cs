﻿using StarmountCore.World.Entities;
using System;
using System.Collections.Generic;
using System.Text;

namespace StarmountServer.Core.Planets.Inhabitants.EventArgs
{
    internal sealed class EntityDestroyedEventArgs
    {
        public DateTime Time { get; }
        public Entity Entity { get; }

        public EntityDestroyedEventArgs(DateTime currentTime, Entity entity)
        {
            Time = currentTime;
            Entity = entity;
        }
    }
}
