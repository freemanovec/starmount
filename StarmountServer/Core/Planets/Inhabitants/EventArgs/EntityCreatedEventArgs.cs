﻿using StarmountCore.World.Entities;
using System;
using System.Collections.Generic;
using System.Text;

namespace StarmountServer.Core.Planets.Inhabitants.EventArgs
{
    internal sealed class EntityCreatedEventArgs
    {
        public DateTime Time { get; }
        public Entity Entity { get; }

        public EntityCreatedEventArgs(DateTime currentTime, Entity entity)
        {
            Time = currentTime;
            Entity = entity;
        }
    }
}
