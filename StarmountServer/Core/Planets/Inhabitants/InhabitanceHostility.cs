using System;
using System.Collections.Generic;
using System.Text;

namespace StarmountServer.Core.Planets.Inhabitants
{
    internal enum InhabitanceHostility
    {
        None,
        Mild,
        Normal,
        Strong,
        Extreme
    }
}
