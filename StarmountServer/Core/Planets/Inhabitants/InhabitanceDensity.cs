﻿using System;
using System.Collections.Generic;
using System.Text;

namespace StarmountServer.Core.Planets.Inhabitants
{
    internal enum InhabitanceDensity
    {
        Empty = 0,
        Scarce = 5,
        SomewhatPopulated = 25,
        Medium = 50,
        VeryPopulated = 60,
        ADamnLot = 70,
        Overcrowded = 85,
        China = 100
    }
}
