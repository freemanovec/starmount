﻿using OpenTK;
using StarmountCore.World.Entities;
using StarmountCore.World.Entities.NPCs;
using StarmountServer.Core.Planets.Inhabitants.EventArgs;
using System;
using System.Collections.Generic;
using System.Text;

namespace StarmountServer.Core.Planets.Inhabitants
{
    internal sealed class InhabitanceManager
    {
        public InhabitanceDensity Density { get; }
        public InhabitanceHostility Hostility { get; }

        public readonly EventHandler<EntityCreatedEventArgs> OnEntityCreatedEventHandler = (s, e) => { };
        public readonly EventHandler<EntityDestroyedEventArgs> OnEntityDestroyedEventHandler = (s, e) => { };
        
        private EntityFactory _factory;
        private List<Entity> _entities = new List<Entity>();

        public InhabitanceManager(InhabitanceConfiguration conf, EntityFactory fact)
        {
            _factory = fact;
            Density = conf.Density;
            Hostility = conf.Hostility;
        }

        public void Initialize()
        {
            // TODO add procedurally generated entities
            for (int i = 0; i < 10; i++)
            {
                float posX = 100f / StarmountCore.Tools.Randomizer.NextUInt16() + 50f;
                Vector2 pos = new Vector2(posX, 400);
                Bloob bloob = _factory.CreateNPCBloob(pos);
                _entities.Add(bloob);
            }
        }

        private void RegisterNewEntity(Entity entity)
        {
            _entities.Add(entity);
            OnEntityCreatedEventHandler(this, new EntityCreatedEventArgs(DateTime.Now, entity));
        }

        private void DestroyEntity(Entity entity)
        {
            if (!_entities.Contains(entity))
                return;
            _entities.Remove(entity);
            OnEntityDestroyedEventHandler(this, new EntityDestroyedEventArgs(DateTime.Now, entity));
        }

        public void Teardown() => _entities.ForEach(e => DestroyEntity(e));

        public void Tick()
        {
            // foreach entity tick
            _entities.ForEach(e => e.Tick());
        }

    }
}
