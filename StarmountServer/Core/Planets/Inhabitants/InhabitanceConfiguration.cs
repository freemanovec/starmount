using System;
using System.Collections.Generic;
using System.Text;

namespace StarmountServer.Core.Planets.Inhabitants
{
    
    internal sealed class InhabitanceConfiguration{

        public static InhabitanceConfiguration Default = new InhabitanceConfiguration(InhabitanceDensity.Medium, InhabitanceHostility.Normal);

        public InhabitanceDensity Density {get;}
        public InhabitanceHostility Hostility {get;}

        public InhabitanceConfiguration(
            InhabitanceDensity density,
            InhabitanceHostility hostility            
        ){
            Density = density;
            Hostility = hostility;
        }

    }

}