﻿using StarmountServer.Core.Chunks;
using System;
using System.Collections.Generic;
using System.Text;

namespace StarmountServer.Core.Planets.Generators
{
    internal interface IChunkSupplier
    {
        Chunk GetChunkAt(int x, int y);
    }
}
