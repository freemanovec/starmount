﻿using System;
using System.Collections.Generic;
using System.Text;
using StarmountCore.Tools;
using StarmountCore.World.Tiles;
using StarmountServer.Core.Chunks;
using StarmountServer.Tools;
using StarmountServer.Tools.Math;

namespace StarmountServer.Core.Planets.Generators {
    class PrimitiveEarth : IChunkSupplier {
        private const double MINIMUM_TERRAIN_HEIGHT = (WorldConstants.MAX_HEIGHT_CHUNKS * WorldConstants.CHUNK_SIZE) * .25f;
        private const double MAXIMUM_TERRAIN_HEIGHT = (WorldConstants.MAX_HEIGHT_CHUNKS * WorldConstants.CHUNK_SIZE) * .35f;
        private const double STEP_ELEVATION = 0.01d;
        private const double STEP_COAL = 0.03d;
        private const double PROBABILITY_COAL = 0.15d;
        private const int COAL_TERRAIN_MIN_PROXIMITY = 25;
        private const int DIRT_DEPTH = 20;
        private const double DIRT_DEPTH_DISTANCE_SCALING = 0.3f;

        private const int NOISE_LEVEL_TERRAIN_HEIGHT = 0;
        private const int NOISE_LEVEL_COAL_PROBABILITY = 1;
        private const int NOISE_LEVEL_DIRT_THICKNESS = 2;

        private ILayeredNoise2D _noise;

        public PrimitiveEarth(
            ILayeredNoise2D noise
            )
        {
            _noise = noise;
        }

        public PrimitiveEarth(int seed) => _noise = new LayeredSimplexNoise2D(seed);

        public PrimitiveEarth() : this(new Random().Next()) { }


        public Chunk GetChunkAt(int x, int y) {
            int worldXPosStart = x * WorldConstants.CHUNK_SIZE;
            int worldXPosEnd = (x + 1) * WorldConstants.CHUNK_SIZE - 1;
            int worldYPosStart = y * WorldConstants.CHUNK_SIZE;
            int worldYPosEnd = (y + 1) * WorldConstants.CHUNK_SIZE - 1;

            Tile[,] tiles = new Tile[WorldConstants.CHUNK_SIZE, WorldConstants.CHUNK_SIZE];

            for (int curX = worldXPosStart; curX <= worldXPosEnd; curX++)
            {
                double elevationX = curX * STEP_ELEVATION;
                double elevationBase = _noise.GetValue(NOISE_LEVEL_TERRAIN_HEIGHT, elevationX, 0);
                double elevationPermutation = _noise.GetValue(NOISE_LEVEL_TERRAIN_HEIGHT, elevationX * 5d, 0) / 10d;
                double elevationRaw = elevationBase + elevationPermutation;
                double norm = (elevationRaw + 1) / 2d;
                int clampedHeight = (int)((MAXIMUM_TERRAIN_HEIGHT - MINIMUM_TERRAIN_HEIGHT) * norm + MINIMUM_TERRAIN_HEIGHT);
                int relX = curX - worldXPosStart;
                for (int curY = worldYPosStart; curY <= worldYPosEnd; curY++)
                {
                    int relY = curY - worldYPosStart;
                    Tile curT = TileHolder.Instance.GetTileForType(TileType.Air);
                    if (curY < clampedHeight)
                    {
                        double coalX = curX * STEP_COAL;
                        double coalY = curY * STEP_COAL;
                        double coalRaw = _noise.GetValue(NOISE_LEVEL_COAL_PROBABILITY, coalX, coalY);
                        bool coal = coalRaw >= (1 - PROBABILITY_COAL) * 2 - 1;
                        if (coal && curY < clampedHeight - COAL_TERRAIN_MIN_PROXIMITY)
                            curT = TileHolder.Instance.GetTileForType(TileType.Coal);
                        else
                        {
                            double dirtDepthRaw = _noise.GetValue(NOISE_LEVEL_DIRT_THICKNESS, elevationX / DIRT_DEPTH_DISTANCE_SCALING, 0);
                            double dirtDepthNormalized = (dirtDepthRaw + 1) / 2 * DIRT_DEPTH;
                            if (curY < clampedHeight - (int)dirtDepthNormalized)
                                curT = TileHolder.Instance.GetTileForType(TileType.Stone);
                            else
                                curT = TileHolder.Instance.GetTileForType(TileType.Dirt);
                        }
                    }else if (curY == clampedHeight)
                        curT = TileHolder.Instance.GetTileForType(TileType.Grass);

                    tiles[relX, relY] = curT;
                }
            }

            return new Chunk(tiles);
        }
    }
}
