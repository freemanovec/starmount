using StarmountCore.World.Physics;
using StarmountServer.Core.Planets.Generators;
using System;
using System.Collections.Generic;
using System.Text;

namespace StarmountServer.Core.Planets{
    internal sealed class PlanetFactory {

        private readonly EngineWrapper _engineWrapper;

        public PlanetFactory(EngineWrapper engineWrapper) => _engineWrapper = engineWrapper;

        public Planet Generate(
            // TODO add some generation options here
        ){
            IChunkSupplier cs = new PrimitiveEarth();
            Fuzzics.Worlds.World world = _engineWrapper.Setup(new Fuzzics.Configurations.GravityConfiguration());
            Planet planet = new Planet(cs, _engineWrapper, world); // TODO select chunk supplier (for now primitive earth, construct and return)
            return planet;
        }

    }
}