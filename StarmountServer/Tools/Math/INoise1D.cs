﻿using System;
using System.Collections.Generic;
using System.Text;

namespace StarmountServer.Tools.Math
{
    internal interface INoise1D
    {
        int Seed { get; set; }
        double GetValue(double x);
    }
}
