﻿using System;
using System.Collections.Generic;
using System.Text;

namespace StarmountServer.Tools.Math
{
    public sealed class SimplexNoise3D : INoise3D
    {
        private int _seed;
        public int Seed
        {
            get => _seed;
            set
            {
                if(value != _seed)
                {
                    Reseed(value);
                    _seed = value;
                }
            }
        }
        private byte[] _hashmap = new byte[CommonTools.HASHMAP_LENGTH];

        public SimplexNoise3D(int seed)
        {
            Seed = seed;
            if (seed == 0) Reseed(seed);
        }

        public double GetValue(double x, double y, double z) => Generate(x, y, z);

        private void Reseed(int seed)
        {
            Random rand = new Random(seed);
            rand.NextBytes(_hashmap);
        }

        private double Generate(double x, double y, double z)
        {
            double n0, n1, n2, n3;

            double s = (x + y + z) * CommonTools.F3;
            double xs = x + s;
            double ys = y + s;
            double zs = z + s;

            int i = CommonTools.FastFloor(xs);
            int j = CommonTools.FastFloor(ys);
            int k = CommonTools.FastFloor(zs);

            double t = (i + j + k) * CommonTools.G3;
            double X0 = i - t;
            double Y0 = j - t;
            double Z0 = k - t;
            double x0 = x - X0;
            double y0 = y - Y0;
            double z0 = z - Z0;

            int i1, j1, k1;
            int i2, j2, k2;

            if (x0 >= y0)
            {
                if (y0 >= z0)
                {
                    i1 = 1;
                    j1 = 0;
                    k1 = 0;
                    i2 = 1;
                    j2 = 1;
                    k2 = 0;
                }
                else if (x0 >= z0)
                {
                    i1 = 1;
                    j1 = 0;
                    k1 = 0;
                    i2 = 1;
                    j2 = 0;
                    k2 = 1;
                }
                else
                {
                    i1 = 0;
                    j1 = 0;
                    k1 = 1;
                    i2 = 1;
                    j2 = 0;
                    k2 = 1;
                }
            }
            else
            {
                if (y0 < z0)
                {
                    i1 = 0;
                    j1 = 0;
                    k1 = 1;
                    i2 = 0;
                    j2 = 1;
                    k2 = 1;
                }
                else if (x0 < z0)
                {
                    i1 = 0;
                    j1 = 1;
                    k1 = 0;
                    i2 = 0;
                    j2 = 1;
                    k2 = 1;
                }
                else
                {
                    i1 = 0;
                    j1 = 1;
                    k1 = 0;
                    i2 = 1;
                    j2 = 1;
                    k2 = 0;
                }
            }

            double x1 = x0 - i1 + CommonTools.G3;
            double y1 = y0 - j1 + CommonTools.G3;
            double z1 = z0 - k1 + CommonTools.G3;
            double x2 = x0 - i2 + 2d * CommonTools.G3;
            double y2 = y0 - j2 + 2d * CommonTools.G3;
            double z2 = z0 - k2 + 2d * CommonTools.G3;
            double x3 = x0 - 1d + 3d * CommonTools.G3;
            double y3 = y0 - 1d + 3d * CommonTools.G3;
            double z3 = z0 - 1d + 3d * CommonTools.G3;

            int ii = CommonTools.Mod(i, 256);
            int jj = CommonTools.Mod(j, 256);
            int kk = CommonTools.Mod(k, 256);

            double t0 = 0.6d - x0 * x0 - y0 * y0 - z0 * z0;
            if (t0 < 0.0d) n0 = 0.0d;
            else
            {
                t0 *= t0;
                n0 = t0 * t0 * CommonTools.Grad(_hashmap[ii + _hashmap[jj + _hashmap[kk]]], x0, y0, z0);
            }

            double t1 = 0.6d - x1 * x1 - y1 * y1 - z1 * z1;
            if (t1 < 0.0d) n1 = 0.0d;
            else
            {
                t1 *= t1;
                n1 = t1 * t1 * CommonTools.Grad(_hashmap[ii + i1 + _hashmap[jj + j1 + _hashmap[kk + k1]]], x1, y1, z1);
            }

            double t2 = 0.6d - x2 * x2 - y2 * y2 - z2 * z2;
            if (t2 < 0.0d) n2 = 0.0d;
            else
            {
                t2 *= t2;
                n2 = t2 * t2 * CommonTools.Grad(_hashmap[ii + i2 + _hashmap[jj + j2 + _hashmap[kk + k2]]], x2, y2, z2);
            }

            double t3 = 0.6d - x3 * x3 - y3 * y3 - z3 * z3;
            if (t3 < 0.0d) n3 = 0.0d;
            else
            {
                t3 *= t3;
                n3 = t3 * t3 * CommonTools.Grad(_hashmap[ii + 1 + _hashmap[jj + 1 + _hashmap[kk + 1]]], x3, y3, z3);
            }
            
            return 32.0d * (n0 + n1 + n2 + n3);
        }

    }
}
