﻿using System;
using System.Collections.Generic;
using System.Text;

namespace StarmountServer.Tools.Math
{
    internal interface INoise3D
    {
        int Seed { get; set; }
        double GetValue(double x, double y, double z);
    }
}
