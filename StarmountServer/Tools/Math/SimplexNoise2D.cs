﻿using System;
using System.Collections.Generic;
using System.Text;

namespace StarmountServer.Tools.Math
{
    public sealed class SimplexNoise2D : INoise2D
    {
        private int _seed;
        public int Seed
        {
            get => _seed;
            set
            {
                if(value != _seed)
                {
                    Reseed(value);
                    _seed = value;
                }
            }
        }
        private byte[] _hashmap = new byte[CommonTools.HASHMAP_LENGTH];

        public SimplexNoise2D(int seed)
        {
            Seed = seed;
            if (seed == 0) Reseed(seed);
        }

        public double GetValue(double x, double y) => Generate(x, y);

        private void Reseed(int seed)
        {
            Random rand = new Random(seed);
            rand.NextBytes(_hashmap);
        }

        private double Generate(double x, double y)
        {
            double n0, n1, n2;

            double s = (x + y) * CommonTools.F2;
            double xs = x + s;
            double ys = y + s;
            int i = CommonTools.FastFloor(xs);
            int j = CommonTools.FastFloor(ys);

            double t = (i + j) * CommonTools.G2;
            double X0 = i - t;
            double Y0 = j - t;
            double x0 = x - X0;
            double y0 = y - Y0;

            int i1, j1;
            if(x0 > y0)
            {
                i1 = 1;
                j1 = 0;
            }
            else
            {
                i1 = 0;
                j1 = 1;
            }

            double x1 = x0 - i1 + CommonTools.G2;
            double y1 = y0 - j1 + CommonTools.G2;
            double x2 = x0 - 1d + 2d * CommonTools.G2;
            double y2 = y0 - 1d + 2d * CommonTools.G2;

            int ii = CommonTools.Mod(i, CommonTools.HASHMAP_LENGTH / 2);
            int jj = CommonTools.Mod(j, CommonTools.HASHMAP_LENGTH / 2);

            double t0 = .5d - x0 * x0 - y0 * y0;
            if (t0 < 0)
                n0 = 0;
            else
            {
                t0 *= t0;
                n0 = t0 * t0 * CommonTools.Grad(_hashmap[ii + _hashmap[jj]], x0, y0);
            }

            double t1 = .5d - x1 * x1 - y1 * y1;
            if (t1 < 0) n1 = 0;
            else
            {
                t1 *= t1;
                n1 = t1 * t1 * CommonTools.Grad(_hashmap[ii + i1 + _hashmap[jj + j1]], x1, y1);
            }

            double t2 = .5d - x2 * x2 - y2 * y2;
            if (t2 < 0) n2 = 0;
            else
            {
                t2 *= t2;
                n2 = t2 * t2 * CommonTools.Grad(_hashmap[ii + 1 + _hashmap[jj + 1]], x2, y2);
            }

            return 40d * (n0 + n1 + n2);
        }

    }
}
