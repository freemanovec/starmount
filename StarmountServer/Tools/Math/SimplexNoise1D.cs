﻿using System;
using System.Collections.Generic;
using System.Text;

namespace StarmountServer.Tools.Math
{
    public sealed class SimplexNoise1D : INoise1D
    {
        private int _seed;
        public int Seed
        {
            get => _seed;
            set
            {
                if(value != _seed)
                {
                    Reseed(value);
                    _seed = value;
                }
            }
        }
        private byte[] _hashmap = new byte[CommonTools.HASHMAP_LENGTH];

        public SimplexNoise1D(int seed)
        {
            Seed = seed;
            if (seed == 0) Reseed(seed);
        }

        public double GetValue(double x) => Generate(x);
        
        private void Reseed(int seed)
        {
            Random rand = new Random(seed);
            rand.NextBytes(_hashmap);
        }

        private double Generate(double x)
        {
            int i0 = CommonTools.FastFloor(x);
            int i1 = i0 + 1;
            double x0 = x - i0;
            double x1 = x0 - 1.0f;

            double n0, n1;

            double t0 = 1.0d - x0 * x0;
            t0 *= t0;
            n0 = t0 * t0 * CommonTools.Grad(_hashmap[i0 & 0xff], x0);

            double t1 = 1.0d - x1 * x1;
            t1 *= t1;
            n1 = t1 * t1 * CommonTools.Grad(_hashmap[i1 & 0xff], x1);

            return 0.395d * (n0 + n1);
        }
    }
}
