﻿using System;
using System.Collections.Generic;
using System.Text;

namespace StarmountServer.Tools.Math
{
    public sealed class LayeredSimplexNoise2D : ILayeredNoise2D
    {
        private const double LAYER_SPACING = 15d;

        public int Seed
        {
            get => _underlyingNoise3D.Seed;
            set => _underlyingNoise3D.Seed = value;
        }

        private SimplexNoise3D _underlyingNoise3D;

        public LayeredSimplexNoise2D(int seed) => _underlyingNoise3D = new SimplexNoise3D(seed);

        public double GetValue(int layer, double x, double y)
        {
            double z = layer * LAYER_SPACING;
            return _underlyingNoise3D.GetValue(x, y, z);
        }
    }
}
