﻿using System;
using System.Collections.Generic;
using System.Text;

namespace StarmountServer.Tools.Math
{
    internal interface ILayeredNoise2D
    {
        int Seed { get; set; }
        double GetValue(int layer, double x, double y);
    }
}
