﻿using System;
using System.Collections.Generic;
using System.Text;

namespace StarmountServer.Tools.Math
{
    internal static class CommonTools
    {
        public const int HASHMAP_LENGTH = 512;

        public static double F2 = (System.Math.Sqrt(3d) - 1d) / 2d;
        public static double G2 = (3d - System.Math.Sqrt(3d)) / 6d;
        public static double F3 = 1d / 3;
        public static double G3 = F3 / 2;

        public static int FastFloor(double x) => x > 0 ? (int)x : ((int)x - 1);

        public static double Grad(int hash, double x)
        {
            int h = hash & 15;
            double grad = 1.0d + (h & 7);
            if ((h & 8) != 0) grad = -grad;
            return grad * x;
        }

        public static double Grad(int hash, double x, double y)
        {
            int h = hash & 7;
            double u = h < 4 ? x : y;
            double v = h < 4 ? y : x;
            return ((h & 1) != 0 ? -u : u) + ((h & 2) != 0 ? -2.0d * v : 2.0d * v);
        }

        public static double Grad(int hash, double x, double y, double z)
        {
            int h = hash & 15;
            double u = h < 8 ? x : y;
            double v = h < 4 ? y : h == 12 || h == 14 ? x : z;
            return ((h & 1) != 0 ? -u : u) + ((h & 2) != 0 ? -v : v);
        }

        public static int Mod(int x, int m)
        {
            int a = x % m;
            return a < 0 ? a + m : a;
        }

    }
}
