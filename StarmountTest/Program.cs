﻿using IDRB.Scenery;
using System;
using System.Diagnostics;
using System.Runtime.CompilerServices;
using System.Threading;
using Fuzzics;
using Fuzzics.Configurations;
using Fuzzics.Worlds;
using IDRB.EventArgs;
using IDRB.GOs;
using OpenTK;
using OpenTK.Graphics;
using StarmountCore.Core;
using System.Collections.Generic;
using StarmountCore.Tools;
using StarmountClient.Networking.TCP;
using StarmountClient.Networking;

namespace StarmountTest
{
    class Program
    {
        static void Main(string[] args)
        {
            ClientEventManager eventManager = new ClientEventManager();
            TcpClient _endpoint = new TcpClient(eventManager);
            _endpoint.Start();
            Thread.Sleep(-1);
        }
    }
}