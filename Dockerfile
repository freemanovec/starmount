FROM microsoft/dotnet-nightly:latest
LABEL maintainer="Freem~ <freemanovec@protonmail.com>"

ADD . /starmount
RUN cd /starmount
RUN find . -name "*.csproj" | xargs -n 1 dotnet restore --packages .nuget/packages
RUN dotnet build /starmount/StarmountServer/StarmountServer.csproj
RUN mkdir -p /root
RUN dotnet publish -o /root /starmount/StarmountServer/StarmountServer.csproj
RUN ls -laR /root && cd /root

ENTRYPOINT [ "dotnet", "/root/StarmountServer.dll", "--verbose" ]