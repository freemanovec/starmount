﻿using StarmountCore.Networking;
using System;
using System.Collections.Generic;
using System.Text;

namespace StarmountClient.Networking
{
    internal interface IClientEndpoint
    {
        void Start(string host, UInt16 port = Constants.DEFAULT_PORT);
        void Stop();
        bool IsConnected { get; }
    }
}
