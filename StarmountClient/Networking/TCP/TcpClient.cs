﻿using System;
using Newtonsoft.Json;
using StarmountCore.Networking;
using StarmountCore.Networking.TCP;

namespace StarmountClient.Networking.TCP
{
    public class TcpClient : IClientEndpoint
    {
        private SimpleTcpClient _client;
        private ClientEventManager _eventManager;

        public bool IsConnected => true; // TODO check for last heartbeat
        
        public TcpClient(ClientEventManager eventManager)
        {
            _eventManager = eventManager;
            _eventManager.PacketProcessor = SendToServer;
        }
        
        public void Start(string host = "localhost", UInt16 port = Constants.DEFAULT_PORT)
        {
            _client = new SimpleTcpClient();
            _client.DelimiterDataReceived += OnMessage;
            _client.Delimiter = (byte)Constants.PROTOCOL_MSG_DELIMITER;
            _client.Connect(host, port);
        }

        public void Stop()
        {
            _client.Disconnect();
        }

        private void OnMessage(object _, Message message)
        {
            Packet requestPacket = JsonConvert.DeserializeObject<Packet>(message.MessageString);
            Packet responsePacket = _eventManager.ProcessIncoming(requestPacket);
            if(!string.IsNullOrEmpty(responsePacket.SerializedRequest))
                message.ReplyLine(JsonConvert.SerializeObject(responsePacket));
        }

        private void SendToServer(object _, Packet requestPacket)
        {
            Message msg = _client.WriteLineAndGetReply(JsonConvert.SerializeObject(requestPacket), TimeSpan.FromSeconds(Constants.TIMEOUT_SECONDS));
            Packet responsePacket = msg == null ? new Packet() { RequestType = requestPacket.ResponseType } : JsonConvert.DeserializeObject<Packet>(msg.MessageString);
            requestPacket.ResponseType = responsePacket.RequestType;
            requestPacket.SerializedResponse = responsePacket.SerializedRequest;
        }
    }
}
