﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Text;
using StarmountCore.Networking;

namespace StarmountClient.Networking
{
    public sealed class CachedNetworkItem<TItem> where TItem : new()
    {
        private TItem _item;
        private DateTime _refreshedAt;
        private EventHandler<TItem> _itemRetrievalEventHandler;

        public CachedNetworkItem(EventHandler<TItem> itemRetrievalEventHandler)
        {
            _item = new TItem();
            _itemRetrievalEventHandler = itemRetrievalEventHandler;
            _refreshedAt = DateTime.MinValue;
        }

        private object _lock = new object();
        public TItem Item
        {
            get
            {
                lock (_lock)
                {
                    if (_refreshedAt.AddSeconds(Constants.CACHED_NETWORK_ITEM_MAX_AGE_SECONDS) < DateTime.Now)
                    {
                        // refresh item
                        Debug.WriteLine($"Refreshing network item {typeof(TItem)} - {_item}");
                        _itemRetrievalEventHandler.Invoke(this, _item);
                        _refreshedAt = DateTime.Now;
                    }

                    return _item;
                }
            }
        }
    }
}
