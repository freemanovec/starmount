﻿using Newtonsoft.Json;
using StarmountCore.Networking;
using System;
using StarmountCore.Exceptions.Runtime;

namespace StarmountClient.Networking
{
    public sealed class ClientEventManager : EventManager
    {
        private object _sendLock = new object();
        public void Send<TRequest, TResponse>(TRequest request, EventHandler<TResponse> onResponse) where TRequest : class where TResponse : new()
        {
            Type requestType = typeof(TRequest);
            Type responseType = typeof(TResponse);
            string serializedRequest = JsonConvert.SerializeObject(request);
            Packet packet = new Packet()
            {
                RequestType = requestType.FullName,
                ResponseType = responseType.FullName,
                SerializedRequest = serializedRequest
            };
            lock(_sendLock)
                PacketProcessor?.Invoke(this, packet);
            if (string.IsNullOrEmpty(packet.SerializedResponse))
                onResponse?.Invoke(this, default(TResponse));
            else
                onResponse?.Invoke(this, JsonConvert.DeserializeObject<TResponse>(packet.SerializedResponse));
        }

    }
}
