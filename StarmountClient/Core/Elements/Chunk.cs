﻿using StarmountCore.World.Tiles;

namespace StarmountClient.Core.Elements
{
    public sealed class Chunk
    {

        public Tile[,] Tiles;
        public int ChunkX { get; }
        public int ChunkY { get; }

        public Tile GetTile(int x, int y) => Tiles[x, y];
        public Tile this[int x, int y] => GetTile(x, y);

        public Chunk()
        {

        }

        public Chunk(Tile[,] tiles, int chunkX, int chunkY)
        {
            Tiles = tiles;
            ChunkX = chunkX;
            ChunkY = chunkY;
        }

        public Chunk(int chunkX, int chunkY) : this(null, chunkX, chunkY)
        {
        }

    }
}
