﻿using StarmountCore.Tools;
using System;
using System.Collections.Generic;
using System.Text;

namespace StarmountClient.Core.Elements
{
    public sealed class Player : Named
    {
        
        public Player(string name, string uuid) : base(name, uuid)
        {
        }

    }
}
