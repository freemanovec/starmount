﻿using StarmountClient.Networking;
using StarmountCore.Core.Actions.Client2Server;
using StarmountCore.Core.Actions.Server2Client;
using StarmountCore.Tools;
using StarmountCore.World.Tiles;
using System;
using System.Collections.Generic;
using System.Text;
using StarmountCore.Exceptions.Networking;
using StarmountCore.Networking;

namespace StarmountClient.Core.Elements
{
    public sealed class Planet : Named
    {
        private ClientEventManager _eventManager;
        private Universe _parentUniverse;
        
        private CachedNetworkItem<List<Player>> _playersCached;
        public List<Player> Players => _playersCached.Item;

        private Dictionary<int, CachedNetworkItem<Chunk>[]> _chunks = new Dictionary<int, CachedNetworkItem<Chunk>[]>();

        public string UniverseUID => _parentUniverse.UID;

        public Planet(Universe universe, ClientEventManager eventManager, string name, string uid) : base(name, uid)
        {
            _parentUniverse = universe;
            _eventManager = eventManager;
        }

        public override string ToString() => $"Planet '{Name}'";

        public Tile GetTile(int _x, int _y) // world coords
        {
            if (_y < 0 || _y >= WorldConstants.MAX_HEIGHT_CHUNKS * WorldConstants.CHUNK_SIZE)
                return TileHolder.Instance.GetTileForType(TileType.Void);
            int chunkX = _x / WorldConstants.CHUNK_SIZE;
            int chunkY = _y / WorldConstants.CHUNK_SIZE;
            int relX = _x % WorldConstants.CHUNK_SIZE;
            if (relX < 0)
            {
                relX = WorldConstants.CHUNK_SIZE + relX;
                chunkX--;
            }
            int relY = _y % WorldConstants.CHUNK_SIZE;
            return GetChunk(chunkX, chunkY).GetTile(relX, relY);
        }

        public Chunk GetChunk(int chunkX, int chunkY) // chunk coords
        {
            if (chunkY < 0 || chunkY >= WorldConstants.MAX_HEIGHT_CHUNKS)
                return new Chunk() {Tiles = ChunkTools.VoidChunkTiles};
            EnsureColumnForChunkX(chunkX);
            return _chunks[chunkX][chunkY].Item;
        }

        private object _ensuranceMutex = new object();
        private void EnsureColumnForChunkX(int chunkX)
        {
            lock (_ensuranceMutex)
            {
                if (_chunks.ContainsKey(chunkX))
                    return;

                CachedNetworkItem<Chunk>[] chnks = new CachedNetworkItem<Chunk>[WorldConstants.MAX_HEIGHT_CHUNKS];
                for (int y = 0; y < chnks.Length; y++)
                {
                    int curY = y;
                    chnks[y] = new CachedNetworkItem<Chunk>((s, chunk) =>
                    {
                        int __y = curY;
                        ChunkRequest request = new ChunkRequest()
                        {
                            UniverseUUID = _parentUniverse.UID,
                            PlanetUUID = UID,
                            ChunkX = chunkX,
                            ChunkY = __y
                        };
                        _eventManager.Send<ChunkRequest, ChunkResponse>(request, (_, chunkResponse) =>
                        {
                            if (NetworkTools.IsEmpty(chunkResponse))
                                throw new NoResponseFromServerException();

                            TileType[,] tileTypes = new TileType[WorldConstants.CHUNK_SIZE, WorldConstants.CHUNK_SIZE];
                            tileTypes = chunkResponse.TileTypes;
                            Tile[,] tiles = new Tile[WorldConstants.CHUNK_SIZE, WorldConstants.CHUNK_SIZE];
                            for (int i = 0; i < WorldConstants.CHUNK_SIZE; i++)
                            for (int j = 0; j < WorldConstants.CHUNK_SIZE; j++)
                            {
                                tiles[i, j] = TileHolder.Instance.GetTileForType(tileTypes[i, j]);
                            }

                            chunk.Tiles = tiles;
                        });
                    });
                }

                _chunks.Add(chunkX, chnks);
            }
        }

    }
}
