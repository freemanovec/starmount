﻿using System;
using System.Collections.Generic;
using System.Text;
using StarmountClient.Networking;
using StarmountCore.Tools;

namespace StarmountClient.Core.Elements
{
    public sealed class Universe : Named
    {
        
        private CachedNetworkItem<List<Planet>> _planetsCached;
        public List<Planet> Planets => _planetsCached.Item;

        public Universe(CachedNetworkItem<List<Planet>> planetsCached, string name, string uid) : base(name, uid)
        {
            _planetsCached = planetsCached;
        }

        public override string ToString() => $"Universe '{Name}'";

    }
}
