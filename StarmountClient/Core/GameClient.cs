using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using StarmountClient.Core.Elements;
using StarmountClient.Networking;
using StarmountClient.Networking.TCP;
using StarmountCore.Core.Actions.Client2Server;
using StarmountCore.Core.Actions.Server2Client;
using StarmountCore.Exceptions.Networking;
using StarmountCore.Networking;
using StarmountCore.Tools;

namespace StarmountClient.Core
{
    public sealed class GameClient
    {
        private ClientEventManager _eventManager;
        private IClientEndpoint _endpoint;
        private Registry<Universe> _registryUniverse;
        private Registry<Planet> _registryPlanet;

        public bool IsConnected => _endpoint.IsConnected;
        public ClientEventManager EventManager => _eventManager;

        public GameClient()
        {
            _eventManager = new ClientEventManager();
            _endpoint = new TcpClient(_eventManager);
            _registryUniverse = new Registry<Universe>();
            _registryPlanet = new Registry<Planet>();

            _universesCached = new CachedNetworkItem<List<Universe>>((s, lst) =>
            {
                _eventManager.Send<UniverseListRequest, UniverseListResponse>(new UniverseListRequest(), (_, universeListResponse) =>
                {
                    if (NetworkTools.IsEmpty(universeListResponse))
                        throw new NoResponseFromServerException();
                    
                    lst.Clear();
                    foreach(UniverseListResponseEntry ulre in universeListResponse.Universes)
                    {
                        Universe universe = _registryUniverse[ulre.UUID];
                        CachedNetworkItem<List<Planet>> cp = new CachedNetworkItem<List<Planet>>((s2, lst_p) =>
                        {
                            _eventManager.Send<PlanetListRequest, PlanetListResponse>(new PlanetListRequest()
                            {
                                UniverseUUID = ulre.UUID
                            }, (_2, planetListResponse) =>
                            {
                                if (NetworkTools.IsEmpty(planetListResponse))
                                    throw new NoResponseFromServerException();

                                lst_p.Clear();
                                foreach(PlanetListResponseEntry plre in planetListResponse.Planets)
                                {
                                    lst_p.Add(new Planet(universe, _eventManager, plre.Name, plre.UUID));
                                }
                            });
                        });
                        if (universe == null)
                            universe = new Universe(cp, ulre.Name, ulre.UUID);
                        lst.Add(universe);
                    }
                });
            });
        }

        public void Connect(string hostname, ushort port)
        {
            IPAddress[] addresses = Dns.GetHostAddresses(hostname).Where(i => i.AddressFamily == System.Net.Sockets.AddressFamily.InterNetwork).ToArray();
            if (addresses.Length < 1)
                throw new InvalidHostnameException();
            Connect(addresses[0], port);
        }

        public void Connect(IPAddress addr, ushort port)
        {
            _endpoint.Start(addr.ToString(), port);
        }

        private CachedNetworkItem<List<Universe>> _universesCached;
        public List<Universe> Universes => _universesCached.Item;

    }
}